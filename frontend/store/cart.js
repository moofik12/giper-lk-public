import {deepCopy} from '../common/utils/helpers';
import CartService from '../common/api/cart.service';
import products from "../common/api/dummy/products";

export const state = () => ({
    products: [],
    saved: {
        carts: []
    },
    current: {
        cart: {
            id: '',
            // editable: true
        }
    }
});

export const mutations = {
    // setEditable(state, flag) {
    //     state.current.cart.editable = flag
    // },
    eraseCart(state) {
        state.products = []
    },
    addToCart(state, products) {
        state.products = products
    },
    deleteProduct(state, index) {
        state.products.splice(index, 1);
    },
    saveCart(state, cart) {
        let copyCart = deepCopy(cart);
        state.saved.carts.push(copyCart)
    },
    setCarts(state, carts) {
        carts.forEach((cart) => {
            cart.cart_details.forEach((detail) => {
                for (let key in detail.product)
                    key.charAt(0) !== '@' ? detail[key] = detail.product[key] : ''
            })
        });
        state.saved.carts = carts
    },
    setLost(state, cart) {
        state.current.cart.id = cart.id;
        state.products = [];
        console.log(cart)
        cart.cart_details.forEach((cart_detail) => {
            cart_detail.product.amount = cart_detail.amount;
            state.products.push(cart_detail.product)
        })
    },
    setCurrent(state, id) {
        state.current.cart.id = id;
    }
};

export const actions = {
    async getLost({commit}) {
        let cart = await CartService.getLost();
        if (!cart.is_deleted) {
            commit('setLost', cart)
        }
    },
    async updateCart({state, commit}, currentProduct) {
        let copyProducts = [];
        if (0 === state.products.length) {
            copyProducts.push(currentProduct);
            let result = await CartService.updateCart(copyProducts, 'post');
            result ? commit('addToCart', copyProducts) : '';
            commit('setCurrent', result['hydra:member'][0].id)

        } else {
            copyProducts = deepCopy(state.products);
            let exist = state.products.findIndex((product) => {
                return currentProduct.article === product.article
            });

            if (exist !== -1) {
                copyProducts[exist].amount += currentProduct.amount
            } else {
                copyProducts.push(currentProduct)
            }
            if (state.current.cart.edit) {
                commit('addToCart', copyProducts)
            } else {
                let result = await CartService.updateCart(copyProducts, 'put');
                result ? commit('addToCart', copyProducts) : ''
            }

        }
    },
    async updateAmount({state, commit}, payload) {
        let copyProducts = deepCopy(state.products);
        copyProducts[payload.indexProduct].amount = payload.amount;
        let result = await CartService.updateCart(copyProducts, 'put');
        result ? commit('addToCart', copyProducts) : ''
    },
    async deleteFromCart({state, commit}, index) {
        if (state.products.length <= 1) {
            await CartService.deleteCart(state.current.cart.id);
            commit('eraseCart');
            return;
        }

        let copyProducts = deepCopy(state.products);
        copyProducts.splice(index, 1);
        let result = await CartService.updateCart(copyProducts, 'put');
        result ? commit('deleteProduct', index) : ''

    },
    async saveCart({commit, state}, name) {
        let result = await CartService.saveCart(name)
    },
    async getSavedCarts({commit}) {
        let carts = await CartService.getSavedCarts();
        if (carts)
            carts = carts.filter(cart => {
                return !cart.is_deleted;
            });
        commit('setCarts', carts)
    },
    async getCart({commit, state}, id) {
        let cart = await CartService.getCart(id);
        if (!cart.is_deleted) {
            commit('setCurrent', cart.id)
            cart.cart_details.forEach(detail => {
                for (let key in detail.product)
                    key.charAt(0) !== '@' ? detail[key] = detail.product[key] : '';
            })
            await CartService.deleteCart(id);
            commit('addToCart', cart.cart_details)
            await CartService.updateCart(state.products, 'post');
        }
    },
    async createOrder({state, commit}, id = '') {
        if (!id) {
            let products = state.saved.carts[id].cart_details;
            let res = await CartService.updateCart(products, 'post');
            let order = await CartService.createOrder();
            return order;
        }
        let order = await CartService.createOrder();
        commit('eraseCart');
        return order;
    },
    async getOrders() {
        let orders = await CartService.getOrders();
        if (orders)
            return orders;
    },
    async deleteCart({dispatch}, id) {
        await CartService.deleteCart(id);
        dispatch('getSavedCarts')
    }
};
