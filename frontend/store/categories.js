import CategoriesService from '../common/api/categories.service';
import { transformToTree } from '../common/utils/categories';

export const state = () => ({
    categories: []
});

export const mutations = {
    setCategories(state, categories) {
        state.categories = categories
    }
};

export const actions = {
    async getCategories ({ commit }) {
        let categories = await CategoriesService.getCategories();

        if (categories['hydra:member']) {
            commit('setCategories', categories['hydra:member']);

            let tree = await transformToTree(categories['hydra:member']);

            return tree;
        }
    }
};