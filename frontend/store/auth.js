import AuthService from '../common/api/auth.service';
import JwtService from '../common/auth/jwt.service';

export const state = () => ({
    login: false,
    token: ''
});

export const mutations = {
    login (state, payload) {
        state.login = payload;
    },

    setToken(state, token) {
        state.token = token;
    }
};

export const actions = {
    async requestToken ({ commit }, credentials) {
        let req;

        try {
            req = await AuthService.getToken(credentials.username, credentials.password);
        } catch (e) {
            return;
        }

        if (req.data.token) {
            JwtService.saveToken(req.data.token);
            commit('login', true);
        }
    }
};
