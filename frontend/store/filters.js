import FilterService from '../common/api/filters.service';

export const state = () => ({
    payload: {
        id: 1,
        page: 1,
        search: '',
        brand: '',
        count: 20,
        range: '',
        actuality: true
    },
    brands: [],
    slider: {}
});

export const mutations = {
    setBrands(state, brands) {
        state.brands = brands
        state.brands.unshift('Все бренды')
    },
    setSlider(state, slider) {
        state.slider = slider
    },
    setPayload(state, data) {
        for (let key in state.payload) {
            if (data.hasOwnProperty(key)) {
                state.payload[key] = data[key]
            }
        }
    }
};

export const getters = {
    getRange(state) {
        return state.slider
    }
};

export const actions = {
    async getBrands({commit}) {
        let brands = await FilterService.getBrands();
        commit('setBrands', brands)
    },
    async getRange({commit}) {
        let slider = await FilterService.getRange();
        commit('setSlider', slider)
    },
    async initFilters({dispatch, commit, state}, filterInitialData) {
        let id = (undefined !== filterInitialData
            && undefined !== filterInitialData.id)
            ? filterInitialData.id
            : state.payload.id;

        let data = await FilterService.initFilters(id);

        let payload = {
            range: [data.price_range.min, data.price_range.max]
        };

        let slider = {
            range: payload.range,
            min: payload.range[0],
            max: payload.range[1]
        };

        commit('setBrands', data.brands);
        commit('setPayload', {...payload, id: id});
        commit('setSlider', slider);

        return state.slider
    }
};
