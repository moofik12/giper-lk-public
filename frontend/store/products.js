import ProductsService from '../common/api/products.service';

export const state = () => ({
    products: [],
    countTotal: 0,
    categoryId: null
});

export const mutations = {
    setProducts(state, products) {
        state.products = products
    },
    setCategoryId(state, categoryId) {
        state.categoryId = categoryId;
    },
    setCountTotal(state, count) {
        state.countTotal = count;
    }
};

export const actions = {
    async getProducts ({ commit }, payload) {
        let products = await ProductsService.getProducts(payload);
        if (products) {
            commit('setProducts', products['hydra:member']);
            commit('setCountTotal', products['hydra:totalItems']);
            commit('setCategoryId', payload.id);
        }
    }
};
