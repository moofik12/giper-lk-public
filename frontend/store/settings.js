import SettingsService from '../common/api/settings.service';

export const state = () => ({
    settings: []
});

export const mutations = {
    setSettings(state, settings) {
        state.settings = settings;
    }
};

export const actions = {
    async getSettings ({ commit }) {
        let settings = await SettingsService.getSettings();

        if (settings) {
            commit('setSettings', settings);
        }
    }
};
