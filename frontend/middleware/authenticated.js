import JwtService from '~/common/auth/jwt.service';
import ApiService from "../common/api/api.service";

export default function ({ store, req, redirect, route }) {
    if (process.server && !req) return;
    // wtf if {req} in getToken ??? check and then delete this comment

    const loggedToken = JwtService.getToken(req);

    if (!loggedToken) {
        return redirect('/');
    }

    if ('/' === route.path && loggedToken) {
        return redirect('/pricelist');
    }

    // ApiService.setHeader(loggedToken);
    store.commit('auth/setToken', loggedToken);
};
