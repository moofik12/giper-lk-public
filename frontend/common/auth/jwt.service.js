import Cookie from 'js-cookie';

const ID_TOKEN_KEY = 'id_token';

export default {
    getToken (context) {
        if (!context.headers.cookie) return null;

        const jwtCookie = context.headers.cookie.split(';').find(c => c.trim().startsWith(ID_TOKEN_KEY + '='))
        if (!jwtCookie) return null;

        return jwtCookie.split('=')[1];
    },

    saveToken (token) {
        if (process.server) return;

        Cookie.set(ID_TOKEN_KEY, token);
    },

    unsetToken () {
        if (process.server) return;

        Cookie.remove(ID_TOKEN_KEY);
    }
};
