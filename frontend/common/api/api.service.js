import axios from 'axios';
import JwtService from '../auth/jwt.service';
import { API_URL } from '../config';

const ApiService = {
    init () {
        axios.defaults.baseURL = API_URL;
    },

    setHeader (token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    },

    query (resource, params) {
        return axios
            .get(resource, params)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            })
    },

    get (resource, slug = '') {
        if ('' !== slug) {
            resource += ('/' + slug);
        }

        return axios
            .get(resource)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            });
    },

    getBlob(url) {
        return axios({
            url: url,
            method: 'GET',
            responseType: 'blob',
        });
    },

    post (resource, params) {
        return axios
            .post(`${resource}`, params)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            });
    },

    update (resource, slug, params) {
        return axios
            .put(`${resource}/${slug}`, params)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            });
    },

    put (resource, params) {
        return axios
            .put(`${resource}`, params)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            });
    },

    delete (resource) {
        return axios
            .delete(resource)
            .catch((error) => {
                throw new Error(`[GLK] ApiService ${error}`)
            })
    }
};

ApiService.init();

export default ApiService;
