import ApiService from './api.service';

const AuthService = {
    getToken (username, password) {
        return ApiService.post('login_check', {"username": username, "password": password});
    },

    checkToken() {
        return ApiService.post('/test');
    }
};

export default AuthService;