import ApiService from './api.service'

const CartService = {
    async getLost() {
        return ApiService.get('/api/carts/lost')
            .then(result => {
                return result.data;
            })
    },
    async deleteCart(id) {
        return ApiService.delete(`/api/carts/${id}`).then(res => {
            return res;
        })
    },
    async updateCart(products, method) {
        let data = {
            products: []
        };

        products.forEach(product => {
            data.products.push({
                article: product.article,
                quantity: product.amount
            })
        });

        return ApiService[method]('/api/carts', data)
            .then(result => {
                return result.data
            })
    },
    async saveCart(name) {
        return ApiService.post('api/carts/save', {name: name}).then(result => {
            return result
        })
    },
    async getSavedCarts() {
        return ApiService.get('api/carts/saved').then(result => {
            return result.data['hydra:member']
        })
    },
    async getCart(id) {
        return ApiService.get(`api/carts/${id}`).then(res => {
            return res.data;
        })
    },
    async createOrder() {
        return ApiService.post('api/orders').then(res => {
            return res;
        })
    },
    async getOrders() {
        return ApiService.get('api/orders').then(res => {
            return res
        })
    }
};

export default CartService;