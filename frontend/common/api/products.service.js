import ApiService from './api.service';

const ProductsService = {
    async getProducts(payload) {
        let id,
            page,
            count,
            search,
            brand,
            priceGT,
            priceLT,
            actuality;

        id = payload.id;
        page = payload.page ? payload.page: null;
        count = payload.count ? payload.count: 20;
        search = payload.search ? '&search=' + payload.search: '';
        brand = payload.brand ? '&brand=' + payload.brand: '';
        priceGT = payload.range[0] ? '&priceThree[gt]=' + payload.range[0]: '';
        priceLT = payload.range[1] ? '&priceThree[lt]=' + payload.range[1]: '';
        actuality = payload.actuality ? '&actuality=' + payload.actuality: '';

        let link = `/api/categories/${id}/nested_products`;

        if (null !== page) {
            link += `?page=${page}&count=${count}${search}${brand}${priceGT}${priceLT}${actuality}`;
        }
        return ApiService.get(link).then(function (result) {
            return result.data;
        })
    }
};

export default ProductsService;
