import ApiServive from '../api/api.service';

const FilterService = {
    async initFilters(category_id) {
        let link = `/api/categories/${category_id}/filter/settings`;
        return ApiServive.get(link).then((result) => {
            return result.data
        })
    }
};

export default FilterService;
