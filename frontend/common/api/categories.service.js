import ApiService from './api.service';

const CategoriesService = {
    async getCategories() {
        return ApiService.get('/api/categories?count=1000').then(function (result) {
            return result.data;
        })
    }
};

export default CategoriesService;