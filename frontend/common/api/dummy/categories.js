const cat = {
    categories: [
        {
            "id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": " Аудио-Видео",
            "parent_id": ""
        },
        {
            "id": "cabb6a1d89db11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Автомобильная техника"
        },
        {
            "id": "cabb6a1e89db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Автомагнитолы"
        },
        {
            "id": "cabb6a2089db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Автомобильные акустические системы"
        },
        {
            "id": "5b6592a077d011e580e144a8420617ac",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Аккумуляторы и пуско-зарядные устройства"
        },
        {
            "id": "cabb6a1f89db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Аксессуары"
        },
        {
            "id": "5f476feb17cd11e4bbbce41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Алкотестеры"
        },
        {
            "id": "cabb6a2189db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Видеорегистраторы"
        },
        {
            "id": "cabb6a2289db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Навигаторы"
        },
        {
            "id": "5f476ff017cd11e4bbbce41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Радар-детекторы"
        },
        {
            "id": "cabb6a2389db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Телевизоры"
        },
        {
            "id": "cabb6a2589db11e394abe41f13bdf060",
            "parent_id": "cabb6a1d89db11e394abe41f13bdf060",
            "title": "Термохолодильники и сумки"
        },
        {
            "id": "64f19ddd89dc11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Аксессуары для аудио-видео"
        },
        {
            "id": "64f19dde89dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "3D очки"
        },
        {
            "id": "64f19ddf89dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Wi-Fi адаптер для ТВ"
        },
        {
            "id": "64f19de589dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Антенны"
        },
        {
            "id": "03c4846189dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Кабели, переходники"
        },
        {
            "id": "8540ed25638311e580d744a8420617ac",
            "parent_id": "03c4846189dc11e394abe41f13bdf060",
            "title": "Аудио"
        },
        {
            "id": "8540ed26638311e580d744a8420617ac",
            "parent_id": "03c4846189dc11e394abe41f13bdf060",
            "title": "Видео"
        },
        {
            "id": "95748b1a638311e580d744a8420617ac",
            "parent_id": "03c4846189dc11e394abe41f13bdf060",
            "title": "Цифровые"
        },
        {
            "id": "64f19de389dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Клавиатуры для ТВ"
        },
        {
            "id": "64f19de489dc11e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Кронштейны"
        },
        {
            "id": "295b59a9c93811e3b2e3e41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Пульты ДУ"
        },
        {
            "id": "cb15ed70e64b11e3b2e3e41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Сетевые фильтры и удлинители"
        },
        {
            "id": "e5c4368fa7d311e580e744a8420617ac",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Чистящие средства"
        },
        {
            "id": "cc3f7fc289d911e394abe41f13bdf060",
            "parent_id": "64f19ddd89dc11e394abe41f13bdf060",
            "title": "Элементы питания"
        },
        {
            "id": "155e7e70761f11e4a782e41f13bdf060",
            "parent_id": "cc3f7fc289d911e394abe41f13bdf060",
            "title": "Аккумуляторы, зарядные устройства и блоки питания"
        },
        {
            "id": "b06ccc84761f11e4a782e41f13bdf060",
            "parent_id": "cc3f7fc289d911e394abe41f13bdf060",
            "title": "Батарейки"
        },
        {
            "id": "cabb6a2a89db11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Аудиотехника"
        },
        {
            "id": "cabb6a2b89db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Hi-Fi, инструменты и оборудование"
        },
        {
            "id": "cabb6a3589db11e394abe41f13bdf060",
            "parent_id": "cabb6a2b89db11e394abe41f13bdf060",
            "title": "Акустические системы"
        },
        {
            "id": "6ff4a5c6f52011e3952ae41f13bdf060",
            "parent_id": "cabb6a2b89db11e394abe41f13bdf060",
            "title": "Инструменты"
        },
        {
            "id": "cabb6a2d89db11e394abe41f13bdf060",
            "parent_id": "cabb6a2b89db11e394abe41f13bdf060",
            "title": "Оборудование"
        },
        {
            "id": "cabb6a2e89db11e394abe41f13bdf060",
            "parent_id": "cabb6a2b89db11e394abe41f13bdf060",
            "title": "Ресиверы и усилители"
        },
        {
            "id": "cabb6a3c89db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Беспроводные аудиосистемы"
        },
        {
            "id": "cabb6a3a89db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Док-станции"
        },
        {
            "id": "cabb6a3289db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Домашние кинотеатры"
        },
        {
            "id": "cabb6a3889db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Магнитолы"
        },
        {
            "id": "cabb6a3989db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Музыкальные центры"
        },
        {
            "id": "cabb6a3f89db11e394abe41f13bdf060",
            "parent_id": "cabb6a3989db11e394abe41f13bdf060",
            "title": "Мидисистемы, Минисистемы"
        },
        {
            "id": "cabb6a3e89db11e394abe41f13bdf060",
            "parent_id": "cabb6a3989db11e394abe41f13bdf060",
            "title": "Микросистемы"
        },
        {
            "id": "cabb6a4389db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Персональное аудио"
        },
        {
            "id": "7680aaad5d0011e8810a44a8420617ac",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Диктофоны"
        },
        {
            "id": "cabb6a4789db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Метеостанции"
        },
        {
            "id": "cabb6a4489db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Микрофоны"
        },
        {
            "id": "cabb6a4589db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Наушники"
        },
        {
            "id": "cabb6a4689db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Плееры"
        },
        {
            "id": "ef72aaadc93311e3b2e3e41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Портативная акустика"
        },
        {
            "id": "cabb6a4989db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Радиобудильники"
        },
        {
            "id": "cabb6a4889db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Радиоприемники"
        },
        {
            "id": "cabb6a4a89db11e394abe41f13bdf060",
            "parent_id": "cabb6a4389db11e394abe41f13bdf060",
            "title": "Синтезаторы"
        },
        {
            "id": "cabb6a3689db11e394abe41f13bdf060",
            "parent_id": "cabb6a2a89db11e394abe41f13bdf060",
            "title": "Саундбары"
        },
        {
            "id": "cabb6a4d89db11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Источники сигнала"
        },
        {
            "id": "03c4845989dc11e394abe41f13bdf060",
            "parent_id": "cabb6a4d89db11e394abe41f13bdf060",
            "title": "BLU-RAY"
        },
        {
            "id": "03c4845b89dc11e394abe41f13bdf060",
            "parent_id": "cabb6a4d89db11e394abe41f13bdf060",
            "title": "DVD"
        },
        {
            "id": "03c4845c89dc11e394abe41f13bdf060",
            "parent_id": "03c4845b89dc11e394abe41f13bdf060",
            "title": "DVD-проигрыватели"
        },
        {
            "id": "03c4845d89dc11e394abe41f13bdf060",
            "parent_id": "03c4845b89dc11e394abe41f13bdf060",
            "title": "DVD-проигрыватели портативные"
        },
        {
            "id": "03c4845e89dc11e394abe41f13bdf060",
            "parent_id": "cabb6a4d89db11e394abe41f13bdf060",
            "title": "Медиаплееры"
        },
        {
            "id": "bf5b25c1ed4b11e3b2e3e41f13bdf060",
            "parent_id": "cabb6a4d89db11e394abe41f13bdf060",
            "title": "Тюнеры DVB-T2"
        },
        {
            "id": "64f19db589dc11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Телевизоры"
        },
        {
            "id": "64f19db989dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 15 - 26\""
        },
        {
            "id": "64f19dc189dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 27 - 29\""
        },
        {
            "id": "64f19dbf89dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 30 - 32\""
        },
        {
            "id": "64f19dc389dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 37 - 43\""
        },
        {
            "id": "64f19dc689dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 44 - 49\""
        },
        {
            "id": "64f19dc989dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 50 - 59\""
        },
        {
            "id": "64f19dcf89dc11e394abe41f13bdf060",
            "parent_id": "64f19db589dc11e394abe41f13bdf060",
            "title": "Телевизоры 60\">"
        },
        {
            "id": "a56b00da89dc11e394abe41f13bdf060",
            "parent_id": "cabb6a1b89db11e394abe41f13bdf060",
            "title": "Фото/камеры"
        },
        {
            "id": "a56b00de89dc11e394abe41f13bdf060",
            "parent_id": "a56b00da89dc11e394abe41f13bdf060",
            "title": "Видеокамеры"
        },
        {
            "id": "a56b00df89dc11e394abe41f13bdf060",
            "parent_id": "a56b00da89dc11e394abe41f13bdf060",
            "title": "Фотоаппараты"
        },
        {
            "id": "a71aee3b213111e4bbbce41f13bdf060",
            "parent_id": "a56b00df89dc11e394abe41f13bdf060",
            "title": "Аксессуары к фотоаппаратам"
        },
        {
            "id": "a56b00e389dc11e394abe41f13bdf060",
            "parent_id": "a56b00df89dc11e394abe41f13bdf060",
            "title": "Зеркальные фотоаппараты"
        },
        {
            "id": "a56b00e289dc11e394abe41f13bdf060",
            "parent_id": "a56b00df89dc11e394abe41f13bdf060",
            "title": "Компактные"
        },
        {
            "id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": " Инструмент и оборудование"
        },
        {
            "id": "c68483a0374d11e8810944a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Бензоинструмент, принадлежности"
        },
        {
            "id": "6fe903b59d4411e7810244a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Запчасти"
        },
        {
            "id": "1fe1bc3a9d4411e7810244a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Расходные материалы, Оснастка"
        },
        {
            "id": "ea812aa29de211e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Аккумуляторы, Зарядные устройства"
        },
        {
            "id": "9cd7288f9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Аксессуары, Принадлежности"
        },
        {
            "id": "7a559c319d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Биты, насадки для шуруповертов"
        },
        {
            "id": "4c5310bb9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Буры, SDS-plus, SDS-max, HEX"
        },
        {
            "id": "5a32e8cb9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Диски и чашки алмазные"
        },
        {
            "id": "6cbccced9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Диски пильные"
        },
        {
            "id": "624902ed9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Коронки сверлильные"
        },
        {
            "id": "4c5310ba9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Круги абразивные по металлу, камню"
        },
        {
            "id": "a6ea73ea9df011e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Наборы разнородной оснастки"
        },
        {
            "id": "210366b62def11e8810844a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Насадки для гравера"
        },
        {
            "id": "3c2556849ded11e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Насадки для мультиинстумента"
        },
        {
            "id": "730329579d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Пики, зубила, штроберы"
        },
        {
            "id": "8380e3289d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Пилки для лобзиков, полотна для пил"
        },
        {
            "id": "44001ea59d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Сверла по металлу, дереву, камню"
        },
        {
            "id": "92304da99d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Фрезы, ножи для рубанка"
        },
        {
            "id": "8b3d73fe9d4511e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Шлифовальная бумага, листы, диски, ленты"
        },
        {
            "id": "e9db04599de411e7810244a8420617ac",
            "parent_id": "1fe1bc3a9d4411e7810244a8420617ac",
            "title": "Щетки проволочные, нейлоновые"
        },
        {
            "id": "0d29c0359d4411e7810244a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Ручной инструмент"
        },
        {
            "id": "345391829d4511e7810244a8420617ac",
            "parent_id": "0d29c0359d4411e7810244a8420617ac",
            "title": "Автомобильный инструмент"
        },
        {
            "id": "288bf3a49d4511e7810244a8420617ac",
            "parent_id": "0d29c0359d4411e7810244a8420617ac",
            "title": "Измерительный инструмент"
        },
        {
            "id": "3a903ffe9d4511e7810244a8420617ac",
            "parent_id": "0d29c0359d4411e7810244a8420617ac",
            "title": "Системы хранения, Верстаки, Лестницы"
        },
        {
            "id": "288bf3a59d4511e7810244a8420617ac",
            "parent_id": "0d29c0359d4411e7810244a8420617ac",
            "title": "Строительный инструмент"
        },
        {
            "id": "65e36d579d4411e7810244a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Садовая техника и инвентарь"
        },
        {
            "id": "bb27b2cb9d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Кусторезы, садовые ножницы "
        },
        {
            "id": "d0e61ac29d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Мойки высокого давления, стеклоочистители, аксессуары"
        },
        {
            "id": "9b41bbd227d311e8810844a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Насосное, поливное оборудование"
        },
        {
            "id": "b4b08ba09d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Пилы цепные, сучкорезы"
        },
        {
            "id": "d75dad8f9d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Расходные материалы и принадлежности "
        },
        {
            "id": "c9d0f0cb9d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Ручной садовый инвентарь"
        },
        {
            "id": "c350a8cb9d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Садовые пылесосы, воздуходувки"
        },
        {
            "id": "acf79adc9d4511e7810244a8420617ac",
            "parent_id": "65e36d579d4411e7810244a8420617ac",
            "title": "Триммеры, Газонокосилки, Аэраторы, Измельчители"
        },
        {
            "id": "f7f138e09d4311e7810244a8420617ac",
            "parent_id": "bf8c6e3b001b11e4b074e41f13bdf060",
            "title": "Электроинструмент"
        },
        {
            "id": "8e3fa3179d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Аккум., Дрели-шуруповерты, Отвертки"
        },
        {
            "id": "9808acbf9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Дрели, Дрели ударные, Миксеры"
        },
        {
            "id": "0af4fe4f9d4511e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Компрессоры воздушные, автомобильные"
        },
        {
            "id": "f1c2202d9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Краскопульты, Распылители"
        },
        {
            "id": "b0ddcadc9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Лобзики, Пилы сабельные (Ножовки)"
        },
        {
            "id": "eacdd53e9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Мультиинструмент, Реноваторы"
        },
        {
            "id": "018a2ba09d4511e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Оборудование сварочное"
        },
        {
            "id": "a86509a49d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Перфораторы, Молотки отбойные"
        },
        {
            "id": "b9177ead9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Пилы циркулярные, торцовочные"
        },
        {
            "id": "120592869d4511e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Прочий электроинструмент"
        },
        {
            "id": "bfcb77829d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Рубанки, Фрезеры"
        },
        {
            "id": "de2311e09d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Точила, Заточные машины"
        },
        {
            "id": "cd7940fe9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "УШМ (Болгарки), Отрезные машины"
        },
        {
            "id": "f98585ed9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Фены, Воздуходувки, Пылесосы"
        },
        {
            "id": "d72ad4f19d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Шлифмашины, Граверы (Бормашины)"
        },
        {
            "id": "9f96a75c9d4411e7810244a8420617ac",
            "parent_id": "f7f138e09d4311e7810244a8420617ac",
            "title": "Шуруповерты, Гайковерты"
        },
        {
            "id": "9b0aa80889d911e394abe41f13bdf060",
            "title": " КБТ"
        },
        {
            "id": "9b0aa7d089d911e394abe41f13bdf060",
            "parent_id": "9b0aa80889d911e394abe41f13bdf060",
            "title": "Встраиваемая техника"
        },
        {
            "id": "9b0aa7d189d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Варочные поверхности"
        },
        {
            "id": "9b0aa7d289d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d189d911e394abe41f13bdf060",
            "title": "Варочные поверхности Domino"
        },
        {
            "id": "9b0aa7d589d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d189d911e394abe41f13bdf060",
            "title": "Варочные поверхности газовые независимые "
        },
        {
            "id": "9b0aa7d689d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d189d911e394abe41f13bdf060",
            "title": "Варочные поверхности индукционные независимые"
        },
        {
            "id": "9b0aa7d789d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d189d911e394abe41f13bdf060",
            "title": "Варочные поверхности комбинированные независимые"
        },
        {
            "id": "9b0aa7d989d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d189d911e394abe41f13bdf060",
            "title": "Варочные поверхности электрические независимые "
        },
        {
            "id": "9b0aa7da89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Вытяжки"
        },
        {
            "id": "9b0aa7db89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7da89d911e394abe41f13bdf060",
            "title": "Вытяжки встраиваемые"
        },
        {
            "id": "9b0aa7dc89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7da89d911e394abe41f13bdf060",
            "title": "Вытяжки каминного типа"
        },
        {
            "id": "9b0aa7dd89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7da89d911e394abe41f13bdf060",
            "title": "Вытяжки подполочные"
        },
        {
            "id": "9b0aa7de89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Духовые шкафы"
        },
        {
            "id": "9b0aa7e089d911e394abe41f13bdf060",
            "parent_id": "9b0aa7de89d911e394abe41f13bdf060",
            "title": "Духовые шкафы газовые независимые "
        },
        {
            "id": "9b0aa7e389d911e394abe41f13bdf060",
            "parent_id": "9b0aa7de89d911e394abe41f13bdf060",
            "title": "Духовые шкафы электрические независимые "
        },
        {
            "id": "9b0aa7e589d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Компактные приборы"
        },
        {
            "id": "9b0aa7ef89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e589d911e394abe41f13bdf060",
            "title": "Компактные духовые шкафы"
        },
        {
            "id": "38f8bc96e1bd11e4baf8e41f13bdf060",
            "parent_id": "9b0aa7e589d911e394abe41f13bdf060",
            "title": "Кофе-машины встраиваемые"
        },
        {
            "id": "9b0aa7f089d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e589d911e394abe41f13bdf060",
            "title": "СВЧ печи встраиваемые"
        },
        {
            "id": "0cb374507a0911e4a921e41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Комплекты зависимые"
        },
        {
            "id": "1cd10bcf764e11e580e144a8420617ac",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Комплекты независимые"
        },
        {
            "id": "9b0aa7e889d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Посудомоечные машины встраиваемые"
        },
        {
            "id": "9b0aa7ec89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e889d911e394abe41f13bdf060",
            "title": "Посудомоечные машины компактные"
        },
        {
            "id": "9b0aa7ea89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e889d911e394abe41f13bdf060",
            "title": "Посудомоечные машины полноразмерные 60 см"
        },
        {
            "id": "9b0aa7e989d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e889d911e394abe41f13bdf060",
            "title": "Посудомоечные машины узкие 45 см"
        },
        {
            "id": "9b0aa7f189d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Стиральные машины встраиваемые"
        },
        {
            "id": "9b0aa7f389d911e394abe41f13bdf060",
            "parent_id": "9b0aa7d089d911e394abe41f13bdf060",
            "title": "Холодильники встраиваемые"
        },
        {
            "id": "922fd1c3e1c711e4baf8e41f13bdf060",
            "parent_id": "9b0aa7f389d911e394abe41f13bdf060",
            "title": "Винные шкафы встраиваемые"
        },
        {
            "id": "9b0aa7f589d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f389d911e394abe41f13bdf060",
            "title": "Морозильные камеры встраиваемые"
        },
        {
            "id": "9b0aa7f489d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f389d911e394abe41f13bdf060",
            "title": "Холодильники без морозильной камеры"
        },
        {
            "id": "9b0aa7f689d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f389d911e394abe41f13bdf060",
            "title": "Холодильники с верхней морозильной камерой"
        },
        {
            "id": "9b0aa7f789d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f389d911e394abe41f13bdf060",
            "title": "Холодильники с нижней морозильной камерой"
        },
        {
            "id": "9b0aa80c89d911e394abe41f13bdf060",
            "parent_id": "9b0aa80889d911e394abe41f13bdf060",
            "title": "Кухонные плиты"
        },
        {
            "id": "9b0aa80e89d911e394abe41f13bdf060",
            "parent_id": "9b0aa80c89d911e394abe41f13bdf060",
            "title": "Плиты газовые "
        },
        {
            "id": "9b0aa80f89d911e394abe41f13bdf060",
            "parent_id": "9b0aa80c89d911e394abe41f13bdf060",
            "title": "Плиты комбинированные"
        },
        {
            "id": "cc3f7fa489d911e394abe41f13bdf060",
            "parent_id": "9b0aa80c89d911e394abe41f13bdf060",
            "title": "Плиты электрические "
        },
        {
            "id": "cc3f7fa789d911e394abe41f13bdf060",
            "parent_id": "9b0aa80889d911e394abe41f13bdf060",
            "title": "Посудомоечные машины"
        },
        {
            "id": "cc3f7faa89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fa789d911e394abe41f13bdf060",
            "title": "Посудомоечные машины настольные"
        },
        {
            "id": "cc3f7fa989d911e394abe41f13bdf060",
            "parent_id": "cc3f7fa789d911e394abe41f13bdf060",
            "title": "Посудомоечные машины полноразмерные 60 см"
        },
        {
            "id": "cc3f7fa889d911e394abe41f13bdf060",
            "parent_id": "cc3f7fa789d911e394abe41f13bdf060",
            "title": "Посудомоечные машины узкие 45 см"
        },
        {
            "id": "cc3f7fae89d911e394abe41f13bdf060",
            "parent_id": "9b0aa80889d911e394abe41f13bdf060",
            "title": "Стиральные машины"
        },
        {
            "id": "f8572587be7611e4ac66e41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины активаторного типа"
        },
        {
            "id": "cc3f7fb489d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины полноразмерные"
        },
        {
            "id": "cc3f7fb189d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины с вертикальной загрузкой"
        },
        {
            "id": "cc3f7fb589d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины с сушкой"
        },
        {
            "id": "cc3f7fb289d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины суперузкие до 40 см"
        },
        {
            "id": "cc3f7fb389d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Стиральные машины узкие 40-50 см"
        },
        {
            "id": "cc3f7fb689d911e394abe41f13bdf060",
            "parent_id": "cc3f7fae89d911e394abe41f13bdf060",
            "title": "Сушильные машины"
        },
        {
            "id": "cc3f7fb789d911e394abe41f13bdf060",
            "parent_id": "9b0aa80889d911e394abe41f13bdf060",
            "title": "Холодильники"
        },
        {
            "id": "02257a96a87011e486e5e41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Винные шкафы"
        },
        {
            "id": "cc3f7fbb89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Морозильные камеры вертикальные"
        },
        {
            "id": "583bec04979d11e7810244a8420617ac",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Морозильные камеры лари"
        },
        {
            "id": "cc3f7fb889d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Холодильники Side by Side"
        },
        {
            "id": "cc3f7fb989d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Холодильники мини бары"
        },
        {
            "id": "cc3f7fba89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Холодильники однокамерные "
        },
        {
            "id": "cc3f7fbc89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Холодильники с верхней морозильной камерой"
        },
        {
            "id": "cc3f7fbd89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fb789d911e394abe41f13bdf060",
            "title": "Холодильники с нижней морозильной камерой"
        },
        {
            "id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": " Климат"
        },
        {
            "id": "9b0aa7fa89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": "Вентиляторы"
        },
        {
            "id": "ec6d042dceb111e3b2e3e41f13bdf060",
            "parent_id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": "Водонагреватели"
        },
        {
            "id": "e34e1fe3b59f11e7810444a8420617ac",
            "parent_id": "ec6d042dceb111e3b2e3e41f13bdf060",
            "title": "Водонагреватель газовый"
        },
        {
            "id": "bb81beab1e9211e780fd44a8420617ac",
            "parent_id": "ec6d042dceb111e3b2e3e41f13bdf060",
            "title": "Водонагреватель накопительный"
        },
        {
            "id": "c936115f1e9211e780fd44a8420617ac",
            "parent_id": "ec6d042dceb111e3b2e3e41f13bdf060",
            "title": "Водонагреватель проточный"
        },
        {
            "id": "9b0aa7fc89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": "Кондиционеры"
        },
        {
            "id": "9b0aa7fd89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7fc89d911e394abe41f13bdf060",
            "title": "Мобильные"
        },
        {
            "id": "9b0aa7fe89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7fc89d911e394abe41f13bdf060",
            "title": "Сплитсистемы"
        },
        {
            "id": "9b0aa7ff89d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": "Обогреватели"
        },
        {
            "id": "9b0aa80289d911e394abe41f13bdf060",
            "parent_id": "9b0aa7ff89d911e394abe41f13bdf060",
            "title": "Конвекторы"
        },
        {
            "id": "ffb0f966870a11e7810144a8420617ac",
            "parent_id": "9b0aa7ff89d911e394abe41f13bdf060",
            "title": "Микатермические обогреватели"
        },
        {
            "id": "2ba69be7645811e680ec44a8420617ac",
            "parent_id": "9b0aa7ff89d911e394abe41f13bdf060",
            "title": "Обогреватели инфракрасные"
        },
        {
            "id": "9b0aa80389d911e394abe41f13bdf060",
            "parent_id": "9b0aa7ff89d911e394abe41f13bdf060",
            "title": "Радиаторы масляные "
        },
        {
            "id": "9b0aa80489d911e394abe41f13bdf060",
            "parent_id": "9b0aa7ff89d911e394abe41f13bdf060",
            "title": "Тепловентиляторы"
        },
        {
            "id": "9b0aa80589d911e394abe41f13bdf060",
            "parent_id": "9b0aa7f889d911e394abe41f13bdf060",
            "title": "Обработка воздуха"
        },
        {
            "id": "9b0aa80689d911e394abe41f13bdf060",
            "parent_id": "9b0aa80589d911e394abe41f13bdf060",
            "title": "Воздухоочистители"
        },
        {
            "id": "9b0aa80789d911e394abe41f13bdf060",
            "parent_id": "9b0aa80589d911e394abe41f13bdf060",
            "title": "Увлажнители"
        },
        {
            "id": "03c4846489dc11e394abe41f13bdf060",
            "title": " Компьютеры и оргтехника"
        },
        {
            "id": "03c4847e89dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "01 Настольные компьютеры"
        },
        {
            "id": "03c4847f89dc11e394abe41f13bdf060",
            "parent_id": "03c4847e89dc11e394abe41f13bdf060",
            "title": "Десктопы"
        },
        {
            "id": "03c4848089dc11e394abe41f13bdf060",
            "parent_id": "03c4847e89dc11e394abe41f13bdf060",
            "title": "Моноблоки"
        },
        {
            "id": "03c4848789dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "02 Портативные компьютеры"
        },
        {
            "id": "03c4848d89dc11e394abe41f13bdf060",
            "parent_id": "03c4848789dc11e394abe41f13bdf060",
            "title": " Ноутбуки"
        },
        {
            "id": "03c4848e89dc11e394abe41f13bdf060",
            "parent_id": "03c4848789dc11e394abe41f13bdf060",
            "title": " Планшеты"
        },
        {
            "id": "03c4848889dc11e394abe41f13bdf060",
            "parent_id": "03c4848789dc11e394abe41f13bdf060",
            "title": "Аксессуары для ноутбуков"
        },
        {
            "id": "10eb0dc9c61311e3b2e3e41f13bdf060",
            "parent_id": "03c4848889dc11e394abe41f13bdf060",
            "title": "Сумки для ноутбуков"
        },
        {
            "id": "03c4848989dc11e394abe41f13bdf060",
            "parent_id": "03c4848789dc11e394abe41f13bdf060",
            "title": "Аксессуары для планшетов"
        },
        {
            "id": "10eb0dcac61311e3b2e3e41f13bdf060",
            "parent_id": "03c4848989dc11e394abe41f13bdf060",
            "title": "Чехлы для планшетов"
        },
        {
            "id": "64f19da989dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "03 Мониторы"
        },
        {
            "id": "64f19daa89dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "04 Устройства печати и копирования"
        },
        {
            "id": "64f19dab89dc11e394abe41f13bdf060",
            "parent_id": "64f19daa89dc11e394abe41f13bdf060",
            "title": "МФУ лазерные"
        },
        {
            "id": "64f19dac89dc11e394abe41f13bdf060",
            "parent_id": "64f19daa89dc11e394abe41f13bdf060",
            "title": "МФУ струйные"
        },
        {
            "id": "64f19dad89dc11e394abe41f13bdf060",
            "parent_id": "64f19daa89dc11e394abe41f13bdf060",
            "title": "Принтеры лазерные"
        },
        {
            "id": "64f19dae89dc11e394abe41f13bdf060",
            "parent_id": "64f19daa89dc11e394abe41f13bdf060",
            "title": "Принтеры струйные"
        },
        {
            "id": "64f19daf89dc11e394abe41f13bdf060",
            "parent_id": "64f19daa89dc11e394abe41f13bdf060",
            "title": "Расходные материалы для печати"
        },
        {
            "id": "03c4849489dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "05 Устройства ввода"
        },
        {
            "id": "03c4849589dc11e394abe41f13bdf060",
            "parent_id": "03c4849489dc11e394abe41f13bdf060",
            "title": "Игровые манипуляторы"
        },
        {
            "id": "03c4849689dc11e394abe41f13bdf060",
            "parent_id": "03c4849489dc11e394abe41f13bdf060",
            "title": "Клавиатуры"
        },
        {
            "id": "03c4849789dc11e394abe41f13bdf060",
            "parent_id": "03c4849489dc11e394abe41f13bdf060",
            "title": "Комплекты (к+м)"
        },
        {
            "id": "03c4849889dc11e394abe41f13bdf060",
            "parent_id": "03c4849489dc11e394abe41f13bdf060",
            "title": "Мыши"
        },
        {
            "id": "03c4847789dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "06 Мультимедиа"
        },
        {
            "id": "03c4847889dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Ак. системы 2.0"
        },
        {
            "id": "03c4847989dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Ак. системы 2.1"
        },
        {
            "id": "03c4847b89dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Игровые приставки"
        },
        {
            "id": "03c4847c89dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Медиаплееры"
        },
        {
            "id": "03c4847d89dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Наушники и микрофоны"
        },
        {
            "id": "478983f49bc511e4bf62e41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Проекторы"
        },
        {
            "id": "03c4847389dc11e394abe41f13bdf060",
            "parent_id": "03c4847789dc11e394abe41f13bdf060",
            "title": "Электронные книги"
        },
        {
            "id": "03c4849189dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "07 Сетевое оборудование"
        },
        {
            "id": "03c4849289dc11e394abe41f13bdf060",
            "parent_id": "03c4849189dc11e394abe41f13bdf060",
            "title": "Беспроводные сети"
        },
        {
            "id": "03c4849389dc11e394abe41f13bdf060",
            "parent_id": "03c4849189dc11e394abe41f13bdf060",
            "title": "Проводные сети"
        },
        {
            "id": "03c4848289dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "08 Носители информации"
        },
        {
            "id": "03c4848389dc11e394abe41f13bdf060",
            "parent_id": "03c4848289dc11e394abe41f13bdf060",
            "title": "Жесткие диски"
        },
        {
            "id": "03c4848489dc11e394abe41f13bdf060",
            "parent_id": "03c4848289dc11e394abe41f13bdf060",
            "title": "Картридеры"
        },
        {
            "id": "03c4848589dc11e394abe41f13bdf060",
            "parent_id": "03c4848289dc11e394abe41f13bdf060",
            "title": "Оптические приводы"
        },
        {
            "id": "03c4848689dc11e394abe41f13bdf060",
            "parent_id": "03c4848289dc11e394abe41f13bdf060",
            "title": "Флешкарты"
        },
        {
            "id": "03c4846789dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "09 Компьютерные аксессуары"
        },
        {
            "id": "64f19de189dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "Веб-камеры"
        },
        {
            "id": "03c4846889dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "Гаджеты USB"
        },
        {
            "id": "03c4846c89dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "ИБП"
        },
        {
            "id": "03c4847089dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "Прочие комп. аксессуары"
        },
        {
            "id": "03c4847189dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "Фильтры сетевые"
        },
        {
            "id": "03c4847289dc11e394abe41f13bdf060",
            "parent_id": "03c4846789dc11e394abe41f13bdf060",
            "title": "Чистящие средства"
        },
        {
            "id": "03c4849089dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "10 Программное обеспечение"
        },
        {
            "id": "a56b00d089dc11e394abe41f13bdf060",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "11 Телефония"
        },
        {
            "id": "a56b00d189dc11e394abe41f13bdf060",
            "parent_id": "a56b00d089dc11e394abe41f13bdf060",
            "title": " Аксессуары для телефонов"
        },
        {
            "id": "a56b00d789dc11e394abe41f13bdf060",
            "parent_id": "a56b00d089dc11e394abe41f13bdf060",
            "title": " Телефоны мобильные"
        },
        {
            "id": "a56b00d689dc11e394abe41f13bdf060",
            "parent_id": "a56b00d089dc11e394abe41f13bdf060",
            "title": "Телефоны беспроводные"
        },
        {
            "id": "a56b00d989dc11e394abe41f13bdf060",
            "parent_id": "a56b00d089dc11e394abe41f13bdf060",
            "title": "Телефоны проводные"
        },
        {
            "id": "9d179bf8010811e8810744a8420617ac",
            "parent_id": "03c4846489dc11e394abe41f13bdf060",
            "title": "акс"
        },
        {
            "id": "cc3f7fbe89d911e394abe41f13bdf060",
            "title": " МБТ"
        },
        {
            "id": "cc3f7fc389d911e394abe41f13bdf060",
            "parent_id": "cc3f7fbe89d911e394abe41f13bdf060",
            "title": "Кухонная техника"
        },
        {
            "id": "cc3f7fc589d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Аксессуары для кухонных приборов"
        },
        {
            "id": "cc3f7fc689d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Аэрогрили"
        },
        {
            "id": "cc3f7fc789d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Блендеры"
        },
        {
            "id": "2ab5d5aac9ed11e3b2e3e41f13bdf060",
            "parent_id": "cc3f7fc789d911e394abe41f13bdf060",
            "title": "Блендеры погружные"
        },
        {
            "id": "2ab5d5abc9ed11e3b2e3e41f13bdf060",
            "parent_id": "cc3f7fc789d911e394abe41f13bdf060",
            "title": "Блендеры стационарные"
        },
        {
            "id": "cc3f7fc889d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Бутербродницы, вафельницы и блинницы"
        },
        {
            "id": "cc3f7fc989d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Весы кухонные"
        },
        {
            "id": "a280471a5fd011e680ec44a8420617ac",
            "parent_id": "cc3f7fc989d911e394abe41f13bdf060",
            "title": "Весы кухонные механические"
        },
        {
            "id": "96e382595fd011e680ec44a8420617ac",
            "parent_id": "cc3f7fc989d911e394abe41f13bdf060",
            "title": "Весы кухонные электронные"
        },
        {
            "id": "cc3f7fca89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Грили"
        },
        {
            "id": "efabd227c37511e580e744a8420617ac",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Дистилляторы"
        },
        {
            "id": "788292e9c9e611e3b2e3e41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Йогуртницы, мороженицы"
        },
        {
            "id": "788292eac9e611e3b2e3e41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Кофе, чай"
        },
        {
            "id": "cc3f7fcf89d911e394abe41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Капельные кофеварки"
        },
        {
            "id": "788292ecc9e611e3b2e3e41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Капсульные кофемашины"
        },
        {
            "id": "457e90678c3511e580e144a8420617ac",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофе в капсулах"
        },
        {
            "id": "3a4cac318c3511e580e144a8420617ac",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофе зерновой"
        },
        {
            "id": "2c8948b272cd11e7810144a8420617ac",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофе молотый"
        },
        {
            "id": "788292ebc9e611e3b2e3e41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофеварки эспрессо"
        },
        {
            "id": "f0164183894d11e680f344a8420617ac",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофеварки-турки и гейзерные"
        },
        {
            "id": "cc3f7fd189d911e394abe41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофемашины"
        },
        {
            "id": "cc3f7fd289d911e394abe41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Кофемолки"
        },
        {
            "id": "295b59abc93811e3b2e3e41f13bdf060",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Фильтры, таблетки, картриджи, аксессуары  для кофеварок"
        },
        {
            "id": "9de4668397cd11e7810244a8420617ac",
            "parent_id": "788292eac9e611e3b2e3e41f13bdf060",
            "title": "Чай"
        },
        {
            "id": "bca413d7b59711e7810444a8420617ac",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Кувшины д/очистки воды, картриджи"
        },
        {
            "id": "cc3f7fd489d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Кухонные машины и комбайны"
        },
        {
            "id": "2bfb3855b55c11e680fb44a8420617ac",
            "parent_id": "cc3f7fd489d911e394abe41f13bdf060",
            "title": "Аксессуары для кухонных машин"
        },
        {
            "id": "cc3f7fcc89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fd489d911e394abe41f13bdf060",
            "title": "Измельчители"
        },
        {
            "id": "ddf75102b55b11e680fb44a8420617ac",
            "parent_id": "cc3f7fd489d911e394abe41f13bdf060",
            "title": "Кухонные комбайны"
        },
        {
            "id": "f85b0de2b55b11e680fb44a8420617ac",
            "parent_id": "cc3f7fd489d911e394abe41f13bdf060",
            "title": "Кухонные машины"
        },
        {
            "id": "cc3f7fd589d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Ломтерезки"
        },
        {
            "id": "cc3f7fe289d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Малые кухонные приборы"
        },
        {
            "id": "cc3f7fd689d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Миксеры"
        },
        {
            "id": "cc3f7fd789d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Минипечи"
        },
        {
            "id": "cc3f7fd889d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Миниплиты"
        },
        {
            "id": "cc3f7fd989d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Мультиварки"
        },
        {
            "id": "bd3a3afdca0a11e3b2e3e41f13bdf060",
            "parent_id": "cc3f7fd989d911e394abe41f13bdf060",
            "title": "Кастрюли для мультиварок"
        },
        {
            "id": "cc3f7fda89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Мясорубки"
        },
        {
            "id": "cc3f7fde89d911e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Пароварки"
        },
        {
            "id": "a9822dc49e5711e580e744a8420617ac",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Приборы для нагрева воды"
        },
        {
            "id": "9aa22f4b846c11e580e144a8420617ac",
            "parent_id": "a9822dc49e5711e580e744a8420617ac",
            "title": "Диспенсеры"
        },
        {
            "id": "22419b3efbf411e580e744a8420617ac",
            "parent_id": "a9822dc49e5711e580e744a8420617ac",
            "title": "Кипятильники"
        },
        {
            "id": "c63b5d8389da11e394abe41f13bdf060",
            "parent_id": "a9822dc49e5711e580e744a8420617ac",
            "title": "Термопоты"
        },
        {
            "id": "d985d6019e5711e580e744a8420617ac",
            "parent_id": "a9822dc49e5711e580e744a8420617ac",
            "title": "Чайники"
        },
        {
            "id": "c63b5d8989da11e394abe41f13bdf060",
            "parent_id": "a9822dc49e5711e580e744a8420617ac",
            "title": "Электрочайники"
        },
        {
            "id": "8c643c2e6e2c11e7810144a8420617ac",
            "parent_id": "c63b5d8989da11e394abe41f13bdf060",
            "title": "Электрочайники металлические"
        },
        {
            "id": "850fe52c6e2c11e7810144a8420617ac",
            "parent_id": "c63b5d8989da11e394abe41f13bdf060",
            "title": "Электрочайники пластиковые"
        },
        {
            "id": "7c75ce526e2c11e7810144a8420617ac",
            "parent_id": "c63b5d8989da11e394abe41f13bdf060",
            "title": "Электрочайники стеклянные"
        },
        {
            "id": "c63b5da789da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "СВЧ печи"
        },
        {
            "id": "c63b5da889da11e394abe41f13bdf060",
            "parent_id": "c63b5da789da11e394abe41f13bdf060",
            "title": "СВЧ печи с грилем"
        },
        {
            "id": "c63b5da989da11e394abe41f13bdf060",
            "parent_id": "c63b5da789da11e394abe41f13bdf060",
            "title": "СВЧ печи с конвекцией"
        },
        {
            "id": "c63b5daa89da11e394abe41f13bdf060",
            "parent_id": "c63b5da789da11e394abe41f13bdf060",
            "title": "СВЧ печи соло"
        },
        {
            "id": "c63b5d8089da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Соковыжималки"
        },
        {
            "id": "2ab5d5a9c9ed11e3b2e3e41f13bdf060",
            "parent_id": "c63b5d8089da11e394abe41f13bdf060",
            "title": "Пресс для цитрусовых"
        },
        {
            "id": "6914111ad9dd11e3b2e3e41f13bdf060",
            "parent_id": "c63b5d8089da11e394abe41f13bdf060",
            "title": "Соковыжималки центробежные"
        },
        {
            "id": "6914111bd9dd11e3b2e3e41f13bdf060",
            "parent_id": "c63b5d8089da11e394abe41f13bdf060",
            "title": "Соковыжималки шнековые"
        },
        {
            "id": "f60be3de01f711e4b074e41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Сушки для продуктов"
        },
        {
            "id": "c63b5d8489da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Тостеры"
        },
        {
            "id": "c63b5d8589da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Фондю"
        },
        {
            "id": "c63b5d8789da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Фритюрницы"
        },
        {
            "id": "c63b5d8889da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Хлебопечи"
        },
        {
            "id": "353800a38c3611e580e144a8420617ac",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Шашлычницы"
        },
        {
            "id": "c63b5d8a89da11e394abe41f13bdf060",
            "parent_id": "cc3f7fc389d911e394abe41f13bdf060",
            "title": "Электрооткрывалки"
        },
        {
            "id": "c63b5d9b89da11e394abe41f13bdf060",
            "parent_id": "cc3f7fbe89d911e394abe41f13bdf060",
            "title": "Уход за домом и одеждой"
        },
        {
            "id": "c63b5dad89da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Аксессуары для глажения"
        },
        {
            "id": "c63b5dae89da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Гладильные доски"
        },
        {
            "id": "9911bbe86a9511e680ee44a8420617ac",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Машинки для стрижки катышков"
        },
        {
            "id": "3d1f9afb675e11e580d744a8420617ac",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Мини-мойки"
        },
        {
            "id": "c63b5db089da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Отпариватели"
        },
        {
            "id": "c63b5daf89da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Паровые станции"
        },
        {
            "id": "c63b5da189da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Пароочистители и паровые швабры"
        },
        {
            "id": "31333f6f97b211e7810244a8420617ac",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Пылесосы"
        },
        {
            "id": "c63b5d9c89da11e394abe41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Аксессуары для пылесосов"
        },
        {
            "id": "b4d446b1ecfb11e580e744a8420617ac",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы автомобильные"
        },
        {
            "id": "bd3a3afbca0a11e3b2e3e41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы беспроводные и вертикальные"
        },
        {
            "id": "c63b5da089da11e394abe41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы моющие"
        },
        {
            "id": "c63b5d9e89da11e394abe41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы с аквафильтром"
        },
        {
            "id": "c63b5d9d89da11e394abe41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы с контейнером для пыли"
        },
        {
            "id": "c63b5d9f89da11e394abe41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы с пылесборником"
        },
        {
            "id": "bd7b789db99e11e3b043e41f13bdf060",
            "parent_id": "31333f6f97b211e7810244a8420617ac",
            "title": "Пылесосы-роботы"
        },
        {
            "id": "c93875ce267b11e680e744a8420617ac",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Сушилки для белья, обуви, рук"
        },
        {
            "id": "c63b5db189da11e394abe41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Утюги"
        },
        {
            "id": "bd3a3afcca0a11e3b2e3e41f13bdf060",
            "parent_id": "c63b5d9b89da11e394abe41f13bdf060",
            "title": "Утюги дорожные"
        },
        {
            "id": "c63b5db289da11e394abe41f13bdf060",
            "parent_id": "cc3f7fbe89d911e394abe41f13bdf060",
            "title": "Уход за собой"
        },
        {
            "id": "c63b5db489da11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Бритье"
        },
        {
            "id": "bd3a3b00ca0a11e3b2e3e41f13bdf060",
            "parent_id": "c63b5db489da11e394abe41f13bdf060",
            "title": "Аксессуары для бритья"
        },
        {
            "id": "da82af8a002d11e4b074e41f13bdf060",
            "parent_id": "c63b5db489da11e394abe41f13bdf060",
            "title": "Бритвы и триммеры женские"
        },
        {
            "id": "acb2a437f53d11e3952ae41f13bdf060",
            "parent_id": "c63b5db489da11e394abe41f13bdf060",
            "title": "Бритвы электрические"
        },
        {
            "id": "c63b5db689da11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Весы напольные"
        },
        {
            "id": "4c8b5cf45fd211e680ec44a8420617ac",
            "parent_id": "c63b5db689da11e394abe41f13bdf060",
            "title": "Весы напольные механические"
        },
        {
            "id": "3f8355fa5fd211e680ec44a8420617ac",
            "parent_id": "c63b5db689da11e394abe41f13bdf060",
            "title": "Весы напольные электронные"
        },
        {
            "id": "c63b5db789da11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Выпрямители"
        },
        {
            "id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Косметические и медицинские приборы"
        },
        {
            "id": "c3e92c3ed0f411e7810644a8420617ac",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Аксессуары д/косметических и медицинских приборов"
        },
        {
            "id": "bd3a3b06ca0a11e3b2e3e41f13bdf060",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Ванночки для ног"
        },
        {
            "id": "c63b5db889da11e394abe41f13bdf060",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Гигиена полости рта"
        },
        {
            "id": "bd3a3affca0a11e3b2e3e41f13bdf060",
            "parent_id": "c63b5db889da11e394abe41f13bdf060",
            "title": "Аксессуары для зубных щеток"
        },
        {
            "id": "acb2a438f53d11e3952ae41f13bdf060",
            "parent_id": "c63b5db889da11e394abe41f13bdf060",
            "title": "Зубные щетки"
        },
        {
            "id": "bd3a3b03ca0a11e3b2e3e41f13bdf060",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Маникюр"
        },
        {
            "id": "bd3a3b04ca0a11e3b2e3e41f13bdf060",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Педикюр"
        },
        {
            "id": "bd3a3b05ca0a11e3b2e3e41f13bdf060",
            "parent_id": "bd3a3b02ca0a11e3b2e3e41f13bdf060",
            "title": "Уход за лицом"
        },
        {
            "id": "c63b5db989da11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Машинки для стрижки и триммеры"
        },
        {
            "id": "c63b5dbd89da11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Стайлеры и щипцы для завивки"
        },
        {
            "id": "cabb6a1089db11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Фен-щетки"
        },
        {
            "id": "cabb6a1189db11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Фены"
        },
        {
            "id": "cabb6a1289db11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Электробигуди"
        },
        {
            "id": "cabb6a1389db11e394abe41f13bdf060",
            "parent_id": "c63b5db289da11e394abe41f13bdf060",
            "title": "Эпиляторы"
        },
        {
            "id": "cabb6a1489db11e394abe41f13bdf060",
            "parent_id": "cc3f7fbe89d911e394abe41f13bdf060",
            "title": "Швейные машины"
        },
        {
            "id": "49311d64fdb011e680fd44a8420617ac",
            "parent_id": "cabb6a1489db11e394abe41f13bdf060",
            "title": "Аксессуары для швейных машин и оверлоков"
        },
        {
            "id": "cabb6a1589db11e394abe41f13bdf060",
            "parent_id": "cabb6a1489db11e394abe41f13bdf060",
            "title": "Оверлоки"
        },
        {
            "id": "cabb6a1689db11e394abe41f13bdf060",
            "parent_id": "cabb6a1489db11e394abe41f13bdf060",
            "title": "Швейные машины"
        },
        {
            "id": "3c1b4d731cf011e680e744a8420617ac",
            "title": " Товары для отдыха и туризма"
        },
        {
            "id": "2c9751581cf111e680e744a8420617ac",
            "parent_id": "3c1b4d731cf011e680e744a8420617ac",
            "title": "Гироскутеры, велосипеды, самокаты"
        },
        {
            "id": "680add340fcd11e780fd44a8420617ac",
            "parent_id": "3c1b4d731cf011e680e744a8420617ac",
            "title": "Коптильни, мангалы, решетки, уголь, жидкость д/розжига"
        },
        {
            "id": "8205caed2d9211e680e744a8420617ac",
            "parent_id": "3c1b4d731cf011e680e744a8420617ac",
            "title": "Мебель для пикника и сада"
        },
        {
            "id": "694f3092617511e7810144a8420617ac",
            "parent_id": "3c1b4d731cf011e680e744a8420617ac",
            "title": "Спиннеры"
        },
        {
            "id": "e04429c92ef411e680e744a8420617ac",
            "parent_id": "3c1b4d731cf011e680e744a8420617ac",
            "title": "Сумки изотермические"
        },
        {
            "id": "4648b0d81cf111e680e744a8420617ac",
            "title": " Хозтовары и предметы интерьера"
        },
        {
            "id": "3b998ba12d9311e680e744a8420617ac",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Борьба с насекомыми"
        },
        {
            "id": "88385be480be11e7810144a8420617ac",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Бытовая химия, тряпки, салфетки"
        },
        {
            "id": "c63b5da489da11e394abe41f13bdf060",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Настенные часы, будильники"
        },
        {
            "id": "93c775d9713611e7810144a8420617ac",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Постельные принадлежности, полотенца"
        },
        {
            "id": "c63b5d8f89da11e394abe41f13bdf060",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Посуда"
        },
        {
            "id": "c63b5d9089da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Аксессуары для кухни"
        },
        {
            "id": "cc3f7fcd89d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Кастрюли"
        },
        {
            "id": "cc3f7fce89d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Ковши"
        },
        {
            "id": "9df03128567f11e7810044a8420617ac",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Контейнеры"
        },
        {
            "id": "63aaf271833f11e7810144a8420617ac",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Кружки, тарелки"
        },
        {
            "id": "cc3f7fd389d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Крышки"
        },
        {
            "id": "cc3f7fdb89d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Набор кухонных принадлежностей"
        },
        {
            "id": "c63b5d9489da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Наборы посуды"
        },
        {
            "id": "c63b5d9589da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Ножи, ножницы, подставки, аксессуары"
        },
        {
            "id": "cc3f7fdf89d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "посуда для сервировки"
        },
        {
            "id": "cc3f7fe089d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Прочая посуда для приготовления"
        },
        {
            "id": "cc3f7fe389d911e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Сковороды"
        },
        {
            "id": "c63b5d8189da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Сотейники"
        },
        {
            "id": "f2dbe368349111e680e744a8420617ac",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Термосы"
        },
        {
            "id": "c63b5d8689da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Формы для выпечки"
        },
        {
            "id": "c63b5d9a89da11e394abe41f13bdf060",
            "parent_id": "c63b5d8f89da11e394abe41f13bdf060",
            "title": "Чайники, кофейники, френч-прессы"
        },
        {
            "id": "5f47701317cd11e4bbbce41f13bdf060",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Свет"
        },
        {
            "id": "ecc521e558f511e480dae41f13bdf060",
            "parent_id": "5f47701317cd11e4bbbce41f13bdf060",
            "title": "Лампы"
        },
        {
            "id": "bd6eb04a1e0411e4bbbce41f13bdf060",
            "parent_id": "5f47701317cd11e4bbbce41f13bdf060",
            "title": "Светильники"
        },
        {
            "id": "5f47701417cd11e4bbbce41f13bdf060",
            "parent_id": "5f47701317cd11e4bbbce41f13bdf060",
            "title": "Фонари"
        },
        {
            "id": "b37a9740fdb311e680fd44a8420617ac",
            "parent_id": "4648b0d81cf111e680e744a8420617ac",
            "title": "Шитье"
        },
        {
            "id": "84543e818a4411e394abe41f13bdf060",
            "title": "Архив"
        },
        {
            "id": "37b95257f45a11e680fd44a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "  Хит-лист !!!"
        },
        {
            "id": "64f19dbc89dc11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "19\"-23\""
        },
        {
            "id": "64f19dc789dc11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "46\"- 49\""
        },
        {
            "id": "cabb6a3389db11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "BLU-RAY домашние кинотеатры"
        },
        {
            "id": "cabb6a3489db11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "DVD домашние кинотеатры"
        },
        {
            "id": "eb2d13a8ce4311e4baf8e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "FIT"
        },
        {
            "id": "a56b00db89dc11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Аксессуары"
        },
        {
            "id": "a56b00dc89dc11e394abe41f13bdf060",
            "parent_id": "a56b00db89dc11e394abe41f13bdf060",
            "title": "аксессуары"
        },
        {
            "id": "9c1d94d7875411e48620e41f13bdf060",
            "parent_id": "a56b00db89dc11e394abe41f13bdf060",
            "title": "Кабели, кронштейны"
        },
        {
            "id": "f8232e6076d611e4a782e41f13bdf060",
            "parent_id": "a56b00db89dc11e394abe41f13bdf060",
            "title": "наушники"
        },
        {
            "id": "cc3f7fc089d911e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Аксессуары для бытовой техники"
        },
        {
            "id": "64f19db889dc11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Архив ЖК"
        },
        {
            "id": "6e2734a4e1e611e580e744a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Архив инструмент"
        },
        {
            "id": "9e553937330611e680e744a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "01 Инструмент DWT"
        },
        {
            "id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "01 Дрели, шуруповерты, перфораторы, винтоверты, гайковерты, отбойные молотки"
        },
        {
            "id": "5a6fc919074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "01 Аккумуляторные дрели-шуроповерты (Li-ion)"
        },
        {
            "id": "5a6fc91a074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "02 Аккумуляторные дрели-шуроповерты (Ni-Cd))"
        },
        {
            "id": "5a6fc91b074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "03 Аккумуляторные винтоверты,гайковерты"
        },
        {
            "id": "5a6fc91c074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "04 Аккумуляторный многофункциональный инструмент"
        },
        {
            "id": "5a6fc91d074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "05 Сетевые шуруповерты"
        },
        {
            "id": "5a6fc922074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "06 Перфораторы"
        },
        {
            "id": "5a6fc923074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "07 Перфораторы тяжелые"
        },
        {
            "id": "5a6fc924074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "08 Отбойные молотки"
        },
        {
            "id": "5a6fc921074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "09 Электродрели ударные"
        },
        {
            "id": "5a6fc91f074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "10 Электродрели"
        },
        {
            "id": "5a6fc920074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "11 Электродрели-миксеры"
        },
        {
            "id": "5a6fc91e074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3c001b11e4b074e41f13bdf060",
            "title": "12 Ударные гайковерты"
        },
        {
            "id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "02 Пилы, электролобзики,электроножовки"
        },
        {
            "id": "5a6fc92e074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "01 Ручные циркулярные пилы"
        },
        {
            "id": "5a6fc933074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "02 Электролобзики"
        },
        {
            "id": "5a6fc934074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "03 Электроножовки"
        },
        {
            "id": "5a6fc92d074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "04 Мраморорезы"
        },
        {
            "id": "5a6fc92f074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "05 Торцовочные пилы"
        },
        {
            "id": "5a6fc930074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "06 Панельные пилы"
        },
        {
            "id": "5a6fc931074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "07 Комбинированные пилы"
        },
        {
            "id": "5a6fc932074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "08 Настольная циркулярная пила"
        },
        {
            "id": "5a6fc92c074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3d001b11e4b074e41f13bdf060",
            "title": "09 Стационарная дисковая отрезная пила"
        },
        {
            "id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "03 Шлифовальные, полировальные машины, заточные станки"
        },
        {
            "id": "5a6fc926074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "01 Углошлифовальные машины"
        },
        {
            "id": "5a6fc928074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "02 Виброшлифовальные машины"
        },
        {
            "id": "5a6fc929074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "03 Эксцентриковые шлифовальные машины"
        },
        {
            "id": "5a6fc92a074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "04 Ленточные шлифовальные машины"
        },
        {
            "id": "5a6fc925074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "05 Прямые шлифовальные машины"
        },
        {
            "id": "5a6fc927074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "06 Полировальные машины"
        },
        {
            "id": "5a6fc92b074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3f001b11e4b074e41f13bdf060",
            "title": "07 Электроточила"
        },
        {
            "id": "bf8c6e40001b11e4b074e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "04 Электрорубанки, электрошаберы, вертикальные фрезерные машины"
        },
        {
            "id": "5a6fc935074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e40001b11e4b074e41f13bdf060",
            "title": "01 Электрорубанки"
        },
        {
            "id": "5a6fc936074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e40001b11e4b074e41f13bdf060",
            "title": "02 Вертикальные фрезерные машины"
        },
        {
            "id": "5a6fc937074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e40001b11e4b074e41f13bdf060",
            "title": "03 Электрошаберы"
        },
        {
            "id": "bf8c6e3e001b11e4b074e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "05 Фены, пылесосы, воздуходувки, пульверизаторы"
        },
        {
            "id": "5a6fc938074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3e001b11e4b074e41f13bdf060",
            "title": "01 Фены промышленные"
        },
        {
            "id": "5a6fc93b074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3e001b11e4b074e41f13bdf060",
            "title": "02 Пульверизаторы"
        },
        {
            "id": "5a6fc939074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3e001b11e4b074e41f13bdf060",
            "title": "03 Универсальные пылесосы"
        },
        {
            "id": "5a6fc93a074311e4b074e41f13bdf060",
            "parent_id": "bf8c6e3e001b11e4b074e41f13bdf060",
            "title": "04 Воздуходувки"
        },
        {
            "id": "0bb3bbc6d39511e4baf8e41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "06 Бензопилы и триммеры"
        },
        {
            "id": "aa6a7145d3aa11e4baf8e41f13bdf060",
            "parent_id": "0bb3bbc6d39511e4baf8e41f13bdf060",
            "title": "01 Бензопилы"
        },
        {
            "id": "e0420633d3aa11e4baf8e41f13bdf060",
            "parent_id": "0bb3bbc6d39511e4baf8e41f13bdf060",
            "title": "02 Бензиновые триммеры"
        },
        {
            "id": "ebcdab41893911e580e144a8420617ac",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "07 Сварочные аппараты"
        },
        {
            "id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "08 Расходные материалы DWT"
        },
        {
            "id": "e755459e6ffa11e4b97fe41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "00 Зарядные устройства, аккумуляторные батареи, патроны для ЭИ"
        },
        {
            "id": "003bb3906ffb11e4b97fe41f13bdf060",
            "parent_id": "e755459e6ffa11e4b97fe41f13bdf060",
            "title": "01 Аккумуляторные батареи"
        },
        {
            "id": "f4c4cc456ffa11e4b97fe41f13bdf060",
            "parent_id": "e755459e6ffa11e4b97fe41f13bdf060",
            "title": "02 Зарядные устройства"
        },
        {
            "id": "086a5c606ffb11e4b97fe41f13bdf060",
            "parent_id": "e755459e6ffa11e4b97fe41f13bdf060",
            "title": "03 Патроны для ЭИ"
        },
        {
            "id": "2dc0de00550f11e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "01 Биты, держатели для бит"
        },
        {
            "id": "56c4af20550f11e480dae41f13bdf060",
            "parent_id": "2dc0de00550f11e480dae41f13bdf060",
            "title": "01 Биты"
        },
        {
            "id": "67ec7800550f11e480dae41f13bdf060",
            "parent_id": "2dc0de00550f11e480dae41f13bdf060",
            "title": "02 Держатели для бит"
        },
        {
            "id": "e0753820550f11e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "02 Сверление"
        },
        {
            "id": "61076d27552211e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "01 Буры"
        },
        {
            "id": "7483de40551011e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "02 Сверла по бетону/камню"
        },
        {
            "id": "44787da0551011e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "03 Сверла по металлу"
        },
        {
            "id": "4ceea0e0551011e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "04 Сверла по дереву"
        },
        {
            "id": "f1bbf2e0550f11e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "05 Универсальные сверла"
        },
        {
            "id": "fed5e440550f11e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "06 Сверла по стеклу/керамике"
        },
        {
            "id": "9c0f2727552511e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "07 Сверлильные коронки"
        },
        {
            "id": "90c4e307552a11e480dae41f13bdf060",
            "parent_id": "e0753820550f11e480dae41f13bdf060",
            "title": "08 Долота с хвостовиком"
        },
        {
            "id": "d2d0a9a7552a11e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "03 Шлифование и полирование"
        },
        {
            "id": "066494c7552b11e480dae41f13bdf060",
            "parent_id": "d2d0a9a7552a11e480dae41f13bdf060",
            "title": "01 Наждачная бумага"
        },
        {
            "id": "0a778f87552c11e480dae41f13bdf060",
            "parent_id": "d2d0a9a7552a11e480dae41f13bdf060",
            "title": "02 Лепестковые шлифовальные диски"
        },
        {
            "id": "8510c0e7552c11e480dae41f13bdf060",
            "parent_id": "d2d0a9a7552a11e480dae41f13bdf060",
            "title": "03 Фибровые шлифовальные диски"
        },
        {
            "id": "f2006d67552a11e480dae41f13bdf060",
            "parent_id": "d2d0a9a7552a11e480dae41f13bdf060",
            "title": "04 Проволочные щетки для УШМ"
        },
        {
            "id": "e51ada47552a11e480dae41f13bdf060",
            "parent_id": "d2d0a9a7552a11e480dae41f13bdf060",
            "title": "05 Проволочные щетки для дрелей"
        },
        {
            "id": "e98f2b47552411e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "04 Резанье и обдирка"
        },
        {
            "id": "fe5c0f27552411e480dae41f13bdf060",
            "parent_id": "e98f2b47552411e480dae41f13bdf060",
            "title": "01 Обрезные и обдирочные диски"
        },
        {
            "id": "1012aa87552511e480dae41f13bdf060",
            "parent_id": "e98f2b47552411e480dae41f13bdf060",
            "title": "02 Алмазные диски"
        },
        {
            "id": "ce6edf67552211e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "05 Пиление"
        },
        {
            "id": "df8ac167552211e480dae41f13bdf060",
            "parent_id": "ce6edf67552211e480dae41f13bdf060",
            "title": "01 Пильные диски"
        },
        {
            "id": "edc4b927552211e480dae41f13bdf060",
            "parent_id": "ce6edf67552211e480dae41f13bdf060",
            "title": "02 Пильные полотна для электролобзиков"
        },
        {
            "id": "fcd28b47552211e480dae41f13bdf060",
            "parent_id": "ce6edf67552211e480dae41f13bdf060",
            "title": "03 Пильные полотна для электроножовок"
        },
        {
            "id": "5a8c7287552f11e480dae41f13bdf060",
            "parent_id": "6e9cf8eb22db11e4bbbce41f13bdf060",
            "title": "06 Фрезерование и строгание"
        },
        {
            "id": "6e68eb87552f11e480dae41f13bdf060",
            "parent_id": "5a8c7287552f11e480dae41f13bdf060",
            "title": "01 Ножи для электрорубанков"
        },
        {
            "id": "5a8c7289552f11e480dae41f13bdf060",
            "parent_id": "5a8c7287552f11e480dae41f13bdf060",
            "title": "02 Фрезы"
        },
        {
            "id": "f79a7e454a2011e480dae41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "09 Запасные части DWT"
        },
        {
            "id": "3adc82ae538711e480dae41f13bdf060",
            "parent_id": "9e553937330611e680e744a8420617ac",
            "title": "10 Стенды DWT"
        },
        {
            "id": "a7fb606cd78211e4baf8e41f13bdf060",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "02 Техника B&D"
        },
        {
            "id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "parent_id": "a7fb606cd78211e4baf8e41f13bdf060",
            "title": "01 Садовая техника B&D"
        },
        {
            "id": "3851b798102f11e5960300145e1b83a2",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "01 Газонокосилки B&D"
        },
        {
            "id": "8324440c102f11e5960300145e1b83a2",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "02 Триммеры B&D"
        },
        {
            "id": "a869c700102f11e5960300145e1b83a2",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "03 Цепные пилы B&D"
        },
        {
            "id": "ef9a7e58102f11e5960300145e1b83a2",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "04 Кусторезы,сучкорезы,садовые ножницы B&D"
        },
        {
            "id": "34b9eba4103011e5960300145e1b83a2",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "05 Садовые пылесосы,аэраторы,измельчители B&D"
        },
        {
            "id": "d66ecac950b711e580d744a8420617ac",
            "parent_id": "693a2de8e1cb11e4baf8e41f13bdf060",
            "title": "06 Принадлежности"
        },
        {
            "id": "f41190fce1cd11e4baf8e41f13bdf060",
            "parent_id": "a7fb606cd78211e4baf8e41f13bdf060",
            "title": "02 Домашняя и автомобильная техника B&D"
        },
        {
            "id": "aef2077e166311e5825000145e1b83a2",
            "parent_id": "f41190fce1cd11e4baf8e41f13bdf060",
            "title": "01 Автомобильная техника"
        },
        {
            "id": "baee79f4166311e5825000145e1b83a2",
            "parent_id": "f41190fce1cd11e4baf8e41f13bdf060",
            "title": "02 Паровая техника"
        },
        {
            "id": "c477e1f4166311e5825000145e1b83a2",
            "parent_id": "f41190fce1cd11e4baf8e41f13bdf060",
            "title": "03 Аккумуляторные пылесосы"
        },
        {
            "id": "9bee54aa008511e5bbf600145e1b83a2",
            "parent_id": "a7fb606cd78211e4baf8e41f13bdf060",
            "title": "03 Инструмент B&D"
        },
        {
            "id": "1d18591215d111e5825000145e1b83a2",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "01 Дрели, шуруповерты, перфораторы, винтоверты"
        },
        {
            "id": "5d8fae9615d111e5825000145e1b83a2",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "02 Шлифовальные машины"
        },
        {
            "id": "be3dea2815d111e5825000145e1b83a2",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "03 Пилы, электролобзики, электрорубанки"
        },
        {
            "id": "dc389bd615d111e5825000145e1b83a2",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "04 Многофункциональный и мультиинструмент"
        },
        {
            "id": "05b1635815d211e5825000145e1b83a2",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "05 Другой инструмент"
        },
        {
            "id": "06d56fb9879511e580e144a8420617ac",
            "parent_id": "9bee54aa008511e5bbf600145e1b83a2",
            "title": "06 Наборы, оснастка для Multievo, расходный материал"
        },
        {
            "id": "26ac6433330811e680e744a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "03 Инструмент Stanley"
        },
        {
            "id": "bc2c3217330811e680e744a8420617ac",
            "parent_id": "26ac6433330811e680e744a8420617ac",
            "title": "01 Электроинструмент Stanley"
        },
        {
            "id": "2d6eebf0330911e680e744a8420617ac",
            "parent_id": "bc2c3217330811e680e744a8420617ac",
            "title": "01 Дрели, шуруповерты"
        },
        {
            "id": "43c0cbb2330911e680e744a8420617ac",
            "parent_id": "bc2c3217330811e680e744a8420617ac",
            "title": "02 Перфораторы, отбойные молотки"
        },
        {
            "id": "711abfa6330911e680e744a8420617ac",
            "parent_id": "bc2c3217330811e680e744a8420617ac",
            "title": "03 Шлифовальные машины"
        },
        {
            "id": "a16ace2c330911e680e744a8420617ac",
            "parent_id": "bc2c3217330811e680e744a8420617ac",
            "title": "04 Пилы, электролобзики, электрорубанки"
        },
        {
            "id": "0120657b330a11e680e744a8420617ac",
            "parent_id": "bc2c3217330811e680e744a8420617ac",
            "title": "05 Другой инструмент"
        },
        {
            "id": "be509d4f19c211e580be44a8420617ac",
            "parent_id": "26ac6433330811e680e744a8420617ac",
            "title": "02 Ручной инструмент Stanley"
        },
        {
            "id": "b799bdfa19c911e580be44a8420617ac",
            "parent_id": "be509d4f19c211e580be44a8420617ac",
            "title": "01 Измерительный инструмент"
        },
        {
            "id": "c7d869b019cb11e580be44a8420617ac",
            "parent_id": "be509d4f19c211e580be44a8420617ac",
            "title": "02 Строительный инструмент"
        },
        {
            "id": "e995409c19cb11e580be44a8420617ac",
            "parent_id": "be509d4f19c211e580be44a8420617ac",
            "title": "03 Хранение"
        },
        {
            "id": "f42bda8f19cb11e580be44a8420617ac",
            "parent_id": "be509d4f19c211e580be44a8420617ac",
            "title": "04 Автомобильный инструмент"
        },
        {
            "id": "258cf084467e11e580d344a8420617ac",
            "parent_id": "26ac6433330811e680e744a8420617ac",
            "title": "03 Расходные материалы и наборы Stanley"
        },
        {
            "id": "f615891a44c211e580d344a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "04 Инструмент DeWalt"
        },
        {
            "id": "19665c5e44c311e580d344a8420617ac",
            "parent_id": "f615891a44c211e580d344a8420617ac",
            "title": "01 Дрели, шуруповерты, перфораторы"
        },
        {
            "id": "2ca0185144c311e580d344a8420617ac",
            "parent_id": "f615891a44c211e580d344a8420617ac",
            "title": "02 Шлифовальные машины"
        },
        {
            "id": "404d4a9444c311e580d344a8420617ac",
            "parent_id": "f615891a44c211e580d344a8420617ac",
            "title": "03 Пилы, электролобзики, электрорубанки"
        },
        {
            "id": "118b6757685e11e680ec44a8420617ac",
            "parent_id": "f615891a44c211e580d344a8420617ac",
            "title": "04 Другой инструмент"
        },
        {
            "id": "4b39bfce686111e680ec44a8420617ac",
            "parent_id": "f615891a44c211e580d344a8420617ac",
            "title": "05 Принадлежности и оснастка"
        },
        {
            "id": "2a703843742f11e680ee44a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "05 Инструмент FIT, Курс, Max-Pro"
        },
        {
            "id": "fb8c99f8ff2b11e680fd44a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "06 Товары Fiskars"
        },
        {
            "id": "a95535e3825811e7810144a8420617ac",
            "parent_id": "fb8c99f8ff2b11e680fd44a8420617ac",
            "title": "01 Товары для сада"
        },
        {
            "id": "563d6e2a825711e7810144a8420617ac",
            "parent_id": "fb8c99f8ff2b11e680fd44a8420617ac",
            "title": "02 Товары для кухни"
        },
        {
            "id": "f9f526eee6c811e680fd44a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "07 Инструмент Kolner и Ставр"
        },
        {
            "id": "39d8565db1da11e49c9fe41f13bdf060",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "08 Инструмент Makita, Интерскол"
        },
        {
            "id": "74d781535a5e11e7810044a8420617ac",
            "parent_id": "6e2734a4e1e611e580e744a8420617ac",
            "title": "09 Техника Karcher"
        },
        {
            "id": "9b0aa80989d911e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Вытяжки"
        },
        {
            "id": "c63b5dac89da11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Глаженье"
        },
        {
            "id": "cc3f7fcb89d911e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "жаровни"
        },
        {
            "id": "c927cf2f875911e48620e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "КБТ"
        },
        {
            "id": "9b0aa7df89d911e394abe41f13bdf060",
            "parent_id": "c927cf2f875911e48620e41f13bdf060",
            "title": "газовые "
        },
        {
            "id": "9b0aa7e189d911e394abe41f13bdf060",
            "parent_id": "c927cf2f875911e48620e41f13bdf060",
            "title": "комбинированные"
        },
        {
            "id": "cc3f7fa589d911e394abe41f13bdf060",
            "parent_id": "c927cf2f875911e48620e41f13bdf060",
            "title": "Морозильники "
        },
        {
            "id": "9b0aa7e689d911e394abe41f13bdf060",
            "parent_id": "c927cf2f875911e48620e41f13bdf060",
            "title": "Морозильники встраиваемые"
        },
        {
            "id": "9b0aa7e789d911e394abe41f13bdf060",
            "parent_id": "9b0aa7e689d911e394abe41f13bdf060",
            "title": "морозильники встраиваемые"
        },
        {
            "id": "0fb6319e843511e48620e41f13bdf060",
            "parent_id": "c927cf2f875911e48620e41f13bdf060",
            "title": "СВЧ встраиваемые"
        },
        {
            "id": "64f19db789dc11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Кинескопные"
        },
        {
            "id": "c2ef61925ae611e580d744a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Климат"
        },
        {
            "id": "f6b45587ebc611e3b2e3e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Колонки"
        },
        {
            "id": "295b59aac93811e3b2e3e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Компьютерные аксессуары"
        },
        {
            "id": "eaa30f2af1c011e580e744a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Магазины"
        },
        {
            "id": "3e40b85c5ab811e580d744a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "МБТ"
        },
        {
            "id": "c63b5dba89da11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "наборы"
        },
        {
            "id": "c3f9f0995f4411e8810a44a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Псков"
        },
        {
            "id": "c63b5dbc89da11e394abe41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Разное"
        },
        {
            "id": "5a5eb32d003411e4b074e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Сетевое оборудование"
        },
        {
            "id": "29845005d46511e3b2e3e41f13bdf060",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Телефоны"
        },
        {
            "id": "c7f51cde23c611e580c644a8420617ac",
            "parent_id": "84543e818a4411e394abe41f13bdf060",
            "title": "Швейки"
        }
    ]
};

export default cat;