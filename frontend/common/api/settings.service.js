import ApiService from './api.service';

const SettingsService = {
    async getSettings() {
        return ApiService.get('/api/user/settings').then(function (result) {
            return result.data;
        })
    }
};

export default SettingsService;