import ApiService from './api.service';

const PricelistService = {
    async getPricelist() {
        let link = '/api/pricelist/feed';

        return ApiService.getBlob(link)
            .then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'price.xml');
                document.body.appendChild(link);
                link.click();
            });
    }
};

export default PricelistService;