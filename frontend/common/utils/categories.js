export function transformToTree(categories) {
    let nodes = [];
    let tree = [];
    categories.forEach(function (category) {
        nodes[category.id] = category;
        nodes[category.id].expand = false;
        nodes[category.id].children = [];
        nodes[category.id].title = category.name;
        delete nodes[category.id].name;
    });

    categories.forEach(function (category) {
            if (nodes[category.parent_id]) {
                nodes[category.parent_id].children.push(category);
            }
    });
    
    for (let key in nodes) {
        if (!nodes[key].parent_id) {
            tree.push(nodes[key])
        }
    }

    return tree;
}