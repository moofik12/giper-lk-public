### Как установить окружение ###
Из корня проекта делаем следующие действия:

1. Нужно установить свежий docker
2. Нужно установить свежий docker-compose
3. git clone git@gitlab.com:moofik12/giper-lk.git
4. Запустить из корня ./docker/script/install.sh
5. Запустить из корня docker/composer install
6. Запустить из корня docker/npm install
7. Запустить из корня docker/psql_init
8. Все dist файлы в папке backend переименовать в те же самые названия без постфикса .dist
9. Запустить из корня docker/up
10. Запустить из корня docker/console doctrine:migrations:migrate
11. Запустить в консоли echo 127.0.0.1 api.giper-lk.local | sudo tee -a /etc/hosts
12. Запустить в консоли echo 127.0.0.1 giper-lk.local | sudo tee -a /etc/hosts

### Как работать ###

В дальнейшем для запуска окружения разработки нужно запускать комманду docker/up
Сервер API будет доступен по адресу api.giper-lk.local
Фронт-энд SPA приложение будет доступно по адресу giper-lk.local
Консольные комманды запускать через docker/console имя_комманды
Для работы с нпм контейнером: скрипт docker/npm
Для работы с компоузер: скрипт docker/composer