#!/usr/bin/env bash
RED='\033[0;31m'
NC='\033[0m'

CREATE_DB_SQL_DIR=$(dirname $0)/../build/postgresql/initdb.d

if [[ "$@" == "--change" ]]; then
	echo -e "${RED}"
	read -p "Enter db Name:" db_name
	read -p "Enter user name:" db_username
	read -p "Enter user pwd:" db_userpwd
	echo -e "${NC}"
	sed -i -E "s/(USER\ +).*?(WITH.*)/\1${db_username} \2/" ${CREATE_DB_SQL_DIR}/create_db.sql
	sed -i -E "s/(PASSWORD\ +).*?(\;.*)/\1'${db_userpwd}'\2/" ${CREATE_DB_SQL_DIR}/create_db.sql
	sed -i -E "s/(CREATE DATABASE\ +).*?(\;.*)/\1${db_name}\2/" ${CREATE_DB_SQL_DIR}/create_db.sql
	sed -i -E "s/(ON DATABASE\ +).*?(\;.*)/\1${db_name} TO ${db_username}\2/" ${CREATE_DB_SQL_DIR}/create_db.sql
elif [[ "$@" == "--help" ]]; then
	echo "Usage: use flag --change after command to regenerate database init script"
else
	echo -e "Use --help for commands"
fi
