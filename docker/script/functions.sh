#!/usr/bin/env bash

DIR=$(readlink -e $(dirname $0))
SUDO_CMD=$(test -w /var/run/docker.sock || echo sudo)
PROJECT_DIR="/giper-lk"
COMPOSER_HOME=${COMPOSER_HOME:-${DIR}/volumes/composer}

php() {
    if [[ ! -d ${COMPOSER_HOME} ]]; then
        mkdir -p ${COMPOSER_HOME}
    fi

    ${SUDO_CMD} docker run \
        -it \
        --rm \
        -v ${COMPOSER_HOME}:${HOME}/.composer \
        -v ${HOME}/.ssh:${HOME}/.ssh \
        -v ${DIR}/../backend:${PROJECT_DIR} \
        -v ${DIR}/volumes/data:/opt/data \
        -v ${DIR}/build/php-cli/php.ini:/etc/php/7.1/cli/php.ini:ro \
        -v /etc/passwd:/etc/passwd:ro \
        -v /etc/group:/etc/group:ro \
        -w ${PROJECT_DIR} \
        -u $(id -u) \
        --network giper-lk \
        giper-lk/php-cli \
        $@
}

node() {
    local base_dir=$(dirname ${DIR})
    local work_dir=$(pwd | sed "s:${base_dir}:${PROJECT_DIR}:")

    if [[ ${work_dir} = $(pwd) ]]; then
        work_dir="${PROJECT_DIR}"
    fi

    if [[ ! -d ${NPM_HOME} ]]; then
        mkdir -p ${NPM_HOME}
    fi

    ${SUDO_CMD} docker run \
        -it \
        --rm \
        -v ${NPM_HOME}:/.npm \
        -v ${NPM_HOME}:/home/node/.npm \
        -v ${HOME}/.ssh:/.ssh \
        -v ${HOME}/.ssh:/home/node/.ssh \
        -v ${DIR}/../frontend:${PROJECT_DIR} \
        -w ${work_dir} \
        -u $(id -u) \
        --network giper-lk \
        node:alpine \
        $@
}

pg() {
    local base_dir=$(dirname ${DIR})
    local work_dir=$(pwd | sed "s:${base_dir}:${PROJECT_DIR}:")

    if [[ ${work_dir} = $(pwd) ]]; then
        work_dir="${PROJECT_DIR}"
    fi

    ${SUDO_CMD} docker run \
        -it \
        --rm \
        -v ${DIR}/../backend:${PROJECT_DIR} \
        -w ${work_dir} \
        --network giper-lk \
        giper-lk/postgresql \
        "$@"
}
