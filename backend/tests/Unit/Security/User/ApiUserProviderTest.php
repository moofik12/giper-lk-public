<?php

namespace App\Test\Unit\Security\User;

use App\Entity\User;
use App\Security\User\ApiUserProvider;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class ApiUserProviderTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp()
    {
        $objectRepository = $this
            ->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();

        $objectRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->entityManager = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->setMethods(['getRepository'])
            ->getMockForAbstractClass();

        $this->entityManager
            ->method('getRepository')
            ->willReturn($objectRepository);

        parent::setUp();
    }

    /**
     * Тест на метод создающий User из респонса API
     */
    public function testLoadUserByApiResponse()
    {
        $apiResponse = [
            'userName' => 'johndoe',
            'userId' => 1,
            'Gender' => 'male',
            'Position' => 'ceo'
        ];

        $apiUserProvider = new ApiUserProvider($this->entityManager);
        /** @var User $user */
        $user = $apiUserProvider->loadUserByApiResponse($apiResponse);

        $this->assertEquals($user->getUsername(), $apiResponse['userName']);
        $this->assertEquals($user->getGender(), $apiResponse['Gender']);
        $this->assertEquals($user->getPosition(), $apiResponse['Position']);
        $this->assertInstanceOf(UserInterface::class, $user);
    }
}