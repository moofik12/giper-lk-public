<?php

namespace App\Test\Unit\Service\Cart;

use App\Entity\Cart;
use App\Entity\CartDetail;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Service\Cart\CartService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CartServiceTest extends TestCase
{
    /**
     * @var CartService|MockObject
     */
    private $cartService;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var CartDetail[]
     */
    private $cartDetails;

    public function setUp()
    {
        parent::setUp();

        $this->cartDetails = [
            $this->createCartDetail(1, 1, 5000, 2),
            $this->createCartDetail(1, 2, 3000, 3)
        ];
        $this->cart = $this->createCart(1, $this->cartDetails);

        $product = $this->createProduct();
        $productRepository = $this->createMock(ProductRepository::class);
        $productRepository
            ->method('find')
            ->willReturn($product);

        /** @var EntityManagerInterface|MockObject $entityManager */
        $entityManager = $this->getMockForAbstractClass(EntityManagerInterface::class);
        $entityManager
            ->method('getRepository')
            ->willReturnMap([
                [Product::class, $productRepository]
            ]);

        $this->cartService = new CartService($entityManager);
    }

    public function getPaymentCostTest()
    {
        $paymentCost = $this->cartService->getPaymentCost($this->cart);

        $this->assertEquals(19000, $paymentCost);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function createCartTest()
    {
        $cartProducts = [
            [
                'id' => 1,
                'quantity' => 2
            ],
            [
                'id' => 2,
                'quantity' => 2
            ]
        ];

        $this->cartService->createCart(new User(), $cartProducts);
    }

    public function updateCartTest()
    {

    }

    /**
     * @param $cartId
     * @param $productId
     * @param $price
     * @param $quantity
     *
     * @return CartDetail
     */
    private function createCartDetail($cartId, $quantity)
    {
        $cartDetail = new CartDetail();

        $cartDetail->setCartId($cartId);
        $cartDetail->setAmount($quantity);

        return $cartDetail;
    }

    /**
     * @param int $userId
     * @param array $cartDetails
     *
     * @return Cart|MockObject
     */
    private function createCart(int $userId, array $cartDetails)
    {
        $cart = $this->createMock(Cart::class);
        $cart->method('getUserId')->willReturn($userId);
        $cart->method('getCartDetails')->willReturn($cartDetails);

        return $cart;
    }
}