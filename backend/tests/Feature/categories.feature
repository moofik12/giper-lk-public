Feature: Categories API methods
  Test for API methods on catalog categories

  @login
  Scenario: Get categories list
    Given I am on test mode
    And there is a category with name "test"
    When I send a GET request to "/api/categories"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json; charset=utf-8"
    And the JSON should be equal to:
    """
      [
          {
              "id": 1,
              "parent_id": null,
              "name": "test"
          }
      ]
    """

  @login
  Scenario: Get existent category
    Given I am on test mode
    And there is a category with name "test"
    When I send a GET request to "/api/categories/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json; charset=utf-8"
    And the JSON should be equal to:
    """
      {
          "id": 1,
          "parent_id": null,
          "name": "test"
      }
    """

  @login
  Scenario: Get non existent category
    Given I am on test mode
    And I send a GET request to "/api/categories/1"
    Then the response status code should be 404
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"