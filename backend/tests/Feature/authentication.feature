Feature: JWT verification
  Test for JWT extension and verification headers

  Scenario: Test protected API URL accessibility without JWT token
    When I send a POST request to "/test"
    Then the response status code should be 401
    And the response should be in JSON
    And the JSON should be equal to:
    """
        {
          "code": 401,
          "message": "JWT Token not found"
        }
    """

  @login
  Scenario: Test protected API URL accessibility with legal JWT token
    When I send a POST request to "/test"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON should be equal to:
    """
        {
          "status": "ok",
          "data": "Token is valid"
        }
    """

  @login
  Scenario: Test protected API URL accessibility with illegal JWT token
    Given there is an illegal JWT token in header
    When I send a POST request to "/test"
    Then the response status code should be 401
    And the response should be in JSON
    And the JSON should be equal to:
    """
        {
          "code": 401,
          "message": "Invalid JWT Token"
        }
    """