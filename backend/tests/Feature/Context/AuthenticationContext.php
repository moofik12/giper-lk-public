<?php

namespace App\Test\Feature\Context;

use App\Entity\User;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behatch\Context\RestContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class AuthenticationContext implements Context
{
    /**
     * @var RestContext $restContext
     */
    private $restContext;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var JWTTokenManagerInterface $jwtManager
     */
    private $jwtManager;

    /**
     * AuthenticationContext constructor.
     *
     * @param ManagerRegistry $doctrine
     * @param JWTTokenManagerInterface $jwtManager
     */
    public function __construct(ManagerRegistry $doctrine, JWTTokenManagerInterface $jwtManager)
    {
        $this->em = $doctrine->getManager('test');
        $this->jwtManager = $jwtManager;
    }

    /**
     * Clears database test data
     *
     * @BeforeScenario
     */
    public function clearData() {
        $purger = new ORMPurger($this->em);
        $purger->purge();
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario @login
     */
    public function login(BeforeScenarioScope $scope)
    {
        $user = new User();
        $user->setUsername('johndoe');
        $user->setPassword('test');
        $user->setGender('male');
        $user->setPosition('CEO');

        $this->em->persist($user);
        $this->em->flush();

        $token = $this->jwtManager->create($user);

        $this->restContext = $scope->getEnvironment()->getContext(RestContext::class);
        $this->restContext->iAddHeaderEqualTo('Authorization', "Bearer $token");
    }

    /**
     * @Given there is an illegal JWT token in header
     */
    public function thereIsAnIllegalJWTTokenInHeader()
    {
        $this->restContext->iAddHeaderEqualTo('Authorization', 'Bearer abcdef');
    }

    /**
     * @AfterScenario @logout
     */
    public function logout() {
        $this->restContext->iAddHeaderEqualTo('Authorization', '');
    }
}
