<?php

namespace App\Test\Feature\Context;

use App\Entity\Category;
use Behat\Behat\Context\Context;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class CatalogContext implements Context
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * CatalogContext constructor.
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager('test');
    }

    /**
     * @Given there is a category with name :name
     *
     * @param string $name
     */
    public function givenThereIsACategoryWithName(string $name)
    {
        $category = new Category();
        $category->setName($name);

        $this->em->persist($category);
        $this->em->flush();
    }
}