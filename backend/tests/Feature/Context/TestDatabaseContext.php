<?php

namespace App\Test\Feature\Context;

use Behatch\Context\BaseContext;
use Symfony\Component\Dotenv\Dotenv;

class TestDatabaseContext extends BaseContext
{
    /**
     * @Given I am on test mode
     */
    public function iAmOnTestMode()
    {
        $dotenv = new Dotenv();
        $dotenv->populate(['TEST_MODE' => true]);
    }
}