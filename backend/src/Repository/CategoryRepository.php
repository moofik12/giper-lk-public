<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param int[] $ids
     *
     * @return array
     */
    public function getPriceRangeById(array $ids): array
    {
        $result = $this->createQueryBuilder('c')
            ->select('MAX(p.priceThree) as max, MIN(p.priceThree) as min')
            ->innerJoin(Product::class,'p', Join::WITH,'p.category=c')
            ->where('c.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getScalarResult();

        return current($result);
    }

    /**
     * @param int[] $ids
     *
     * @return array
     */
    public function getBrandsByIds(array $ids): array
    {
        $result = $this->createQueryBuilder('c')
            ->select('DISTINCT p.brand')
            ->innerJoin(Product::class,'p', Join::WITH,'p.category=c AND p.brand IS NOT NULL')
            ->where('c.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('p.brand', 'ASC')
            ->getQuery()
            ->getScalarResult();

        return array_column($result, 'brand');
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllCategoryChildren(int $id)
    {
        $getCategoryAndItsParentsChildrenIdsQuery = <<<SQL
WITH RECURSIVE r AS (
  SELECT id, parent_id
  FROM categories
  WHERE id = :categoryId
  UNION
  SELECT categories.id, categories.parent_id
  FROM categories
  JOIN r ON r.id = categories.parent_id
)
SELECT id FROM r;
SQL;
        /** @noinspection PhpUnhandledExceptionInspection */
        $statement = $this->getEntityManager()
            ->getConnection()
            ->prepare($getCategoryAndItsParentsChildrenIdsQuery);
        $statement->bindValue(':categoryId', $id);
        $statement->execute();

        /** @var array $categoryAndItsParentsIds */
        return $statement->fetchAll(FetchMode::COLUMN);
    }
}
