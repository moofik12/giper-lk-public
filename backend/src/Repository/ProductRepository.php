<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findExistentArticles(array $articles)
    {
        $result = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p.article')
            ->from(Product::class, 'p')
            ->andWhere('p.article IN (:articles)')
            ->setParameter('articles', $articles)
            ->getQuery()
            ->getScalarResult();

        return array_column($result, 'article');
    }
}
