<?php

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cart[]    findAll()
 * @method Cart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cart::class);
    }

    /**
     * @var User $user
     *
     * @return Cart|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastCreatedCart(User $user): ?Cart
    {
        return $this->createQueryBuilder('c')
            ->where('c.user=:user')
            ->setParameter('user', $user)
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @var User $user
     *
     * @return Cart[]
     */
    public function findAllSavedByUser(User $user): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.user=:user')
            ->andWhere('c.isSaved=TRUE')
            ->setParameter('user', $user)
            ->getQuery()
            ->setFetchMode(Cart::class, 'cartDetails', ClassMetadata::FETCH_EAGER)
            ->getResult();
    }

    /**
     * @var User $user
     *
     * @return Cart[]
     */
    public function findAllOrderedByUser(User $user): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.user=:user')
            ->andWhere('c.isOrdered=:true')
            ->setParameter('true', true)
            ->setParameter('user', $user)
            ->getQuery()
            ->setFetchMode(Cart::class, 'cartDetails', ClassMetadata::FETCH_EAGER)
            ->getResult();
    }

    /**
     * @param User $user
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLostCart(User $user): ?Cart
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user=:user')
            ->andWhere('c.isOrdered=:ordered')
            ->setParameter('ordered', false)
            ->setParameter('user', $user)
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->setFetchMode(Cart::class, 'cartDetails', ClassMetadata::FETCH_EAGER)
            ->getOneOrNullResult();
    }
}
