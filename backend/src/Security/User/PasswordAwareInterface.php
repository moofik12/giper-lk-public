<?php

namespace App\Security\User;

interface PasswordAwareInterface
{
    /**
     * @param string $password
     */
    public function setUsername(string $password);

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @param string $password
     */
    public function setPassword(string $password);

    /**
     * @return string
     */
    public function getPassword(): string;
}
