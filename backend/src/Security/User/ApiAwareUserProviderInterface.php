<?php

namespace App\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

interface ApiAwareUserProviderInterface
{
    /**
     * Извлекает пользователя из тела ответа API
     *
     * @param array $response - тело ответа API
     *
     * @return UserInterface|null
     */
    public function loadUserByApiResponse(array $response): ?UserInterface;
}
