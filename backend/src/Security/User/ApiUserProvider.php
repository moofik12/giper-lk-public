<?php

namespace App\Security\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiUserProvider implements UserProviderInterface, ApiAwareUserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ApiUserProvider constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     * @throws UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }

    /**
     * @param string $username
     *
     * @return UserInterface
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username
            ]);

        if ($user) {
            return $user;
        }

        throw new UsernameNotFoundException();
    }

    /**
     * @param array $response
     *
     * @return UserInterface
     */
    public function loadUserByApiResponse(array $response): UserInterface
    {
        if (isset($response[0]['UserName'])
            && isset($response[0]['UserID'])
            && isset($response[0]['Gender'])
            && isset($response[0]['Position'])
        ) {
            $user = $this->em
                ->getRepository(User::class)
                ->findOneBy([
                    'userErpId' => $response[0]['UserID']
                ]);

            /** @var User $user */
            if ($user) {
                return $user;
            }

            $user = new User();
            $user->setUserErpId($response[0]['UserID']);
            $user->setPosition($response[0]['Position']);
            $user->setGender($response[0]['Gender']);

            return $user;
        }
    }
}