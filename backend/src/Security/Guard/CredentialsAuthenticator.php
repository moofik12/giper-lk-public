<?php

namespace App\Security\Guard;

use App\Entity\User;
use App\Security\User\ApiAwareUserProviderInterface;
use App\Security\User\NotImplementedException;
use App\Security\User\PasswordAwareInterface;
use App\Service\ERP\ERPApiException;
use App\Service\ERP\ERPApiInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\Response;

class CredentialsAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var ERPApiInterface
     */
    protected $ERPApi;

    /**
     * @var PasswordEncoderInterface
     */
    protected $encoder;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * CredentialsAuthenticator constructor.
     *
     * @param ERPApiInterface $ERPApi
     * @param PasswordEncoderInterface $encoder
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ERPApiInterface $ERPApi,
        PasswordEncoderInterface $encoder,
        EntityManagerInterface $entityManager
    ) {
        $this->ERPApi = $ERPApi;
        $this->encoder = $encoder;
        $this->em = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        return [
            'username' => $data['username'],
            'password' => $data['password'],
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return UserInterface
     * @throws NotImplementedException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            if ($userProvider instanceof ApiAwareUserProviderInterface) {
                $passwordHash = $this->encoder->encodePassword($credentials['password'], '');
                $apiResponse = $this->ERPApi->getUser($credentials['username'], $passwordHash);
                /** @var User $user */
                $user = $userProvider->loadUserByApiResponse($apiResponse);

                if ($user instanceof PasswordAwareInterface) {
                    $user->setUsername($credentials['username']);
                    $user->setPassword($passwordHash);
                    $this->em->persist($user);
                    $this->em->flush();

                    return $user;
                }

                throw new NotImplementedException('PasswordAwareInterface not implemented');
            }

            throw new NotImplementedException('ApiAwareUserProviderInterface not implemented');
        } catch (ERPApiException $exception) {
            throw new UsernameNotFoundException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return new UsernamePasswordToken(
            $user,
            ['username' => $user->getUsername(), 'password' => $user->getPassword()],
            'main',
            $user->getRoles()
        );
    }
}