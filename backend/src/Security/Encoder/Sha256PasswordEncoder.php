<?php

namespace App\Security\Encoder;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class Sha256PasswordEncoder implements PasswordEncoderInterface
{
    public function encodePassword($raw, $salt)
    {
        return hash('sha256', $raw);
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === hash('sha256', $raw);
    }
}