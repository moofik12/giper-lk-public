<?php

namespace App\Command;

use App\Service\Import\XmlPricelistImporter;
use App\Service\Import\XmlPricelistParser;
use App\Service\Import\XmlPricelistParserFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppImportPricelistCommand extends Command
{
    /**
     * @var XmlPricelistParser
     */
    protected $parser;

    /**
     * @var XmlPricelistImporter
     */
    protected $importer;

    protected static $defaultName = 'app:import:pricelist';

    /**
     * AppImportPricelistCommand constructor.
     *
     * @param XmlPricelistParserFactory $parserFactory
     * @param XmlPricelistImporter $importer
     */
    public function __construct(
        XmlPricelistParserFactory $parserFactory,
        XmlPricelistImporter $importer
    )
    {
        $this->parser = $parserFactory->create('var/storage/pricelist.xml');
        $this->importer = $importer;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:import:pricelist');
        $this->setDescription('Import pricelist with categories and products');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $io = new SymfonyStyle($input, $output);
       $io->write('Price list import start');

       $this->importer->importFromFile($this->parser, 1000);

       $io->success('Price list imported successfully.');
    }
}