<?php

namespace App\Command;

use Ijanki\Bundle\FtpBundle\Ftp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Psr\Log\LoggerInterface;

class AppDownloadPricelistsCommand extends Command
{
    /**
     * @var
     */
    protected $host;

    /**
     * @var
     */
    protected $login;

    /**
     * @var
     */
    protected $password;

    /**
     * @var string
     */
    protected $rootDir = '';

    /**
     * @var array
     */
    protected $fileNames = [];

    /**
     * @var
     */
    protected $io;

    /**
     * @var \ZipArchive
     */
    protected $zip;

    /**
     * @var Ftp
     */
    protected $ftpService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected static $defaultName = 'app:download:pricelist';

    /**
     * AppDownloadPricelistsCommand constructor.
     * @param Ftp $ftpService
     * @param LoggerInterface $logger
     * @param $rootDir
     * @param $giperFtp
     * @param $prices
     */
    public function __construct(Ftp $ftpService, LoggerInterface $logger, $rootDir, $giperFtp, $prices)
    {
        $this->ftpService = $ftpService;
        $this->logger = $logger;
        $this->rootDir = $rootDir . '/var/storage/';
        $this->host = $giperFtp['host'];
        $this->login = $giperFtp['login'];
        $this->password = $giperFtp['password'];
        $this->fileNames = $prices;
        $this->zip = new \ZipArchive();
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Download and unpacked pricelists from ftp server');
    }

    protected function downloadPriceLists()
    {
        $remoteFiles = $this->ftpService->nlist('.');
        foreach ($this->fileNames as $file) {
            if (in_array($file, $remoteFiles)) {
                $this->ftpService->get($this->rootDir . $file, '/' . $file, FTP_BINARY);
            } else {
                $this->logger->warning('file ' . $file . ' not found');
            }
        }
    }

    protected function unzip()
    {
        foreach ($this->fileNames as $file) {
            $errorCode = $this->zip->open($this->rootDir . $file);
            if (true === $errorCode) {
                $this->io->progressStart(100);
                $this->zip->extractTo($this->rootDir);
                $this->io->progressFinish();
                $this->zip->close();
                $this->io->success($file . ' successfully unpacked');
            } else {
                throw new \RuntimeException('ZipArchive error code ' . $errorCode);
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->write('the connection to the ftp server ' . $this->host);
        $this->io->newLine();
        $this->ftpService->connect($this->host);
        $connect = $this->ftpService->login($this->login, $this->password);
        $this->ftpService->pasv(true);

        if ($connect) {
            $this->io->write('the connection is successful');
            $this->downloadPriceLists();
            $this->unzip();
        }
    }
}
