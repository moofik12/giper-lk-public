<?php

namespace App\EventSubscriber;

use App\Entity\Cart;
use App\Entity\CartDetail;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\ERP\ERPApiException;
use App\Service\ERP\ERPApiInterface;
use App\Service\ERP\Product\ProductValidator;
use App\Service\ERP\Product\ProductValidatorException;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\OrderCreatedEvent;

class OrderCreatedSubscriber implements EventSubscriberInterface
{
    /**
     * @var ERPApiInterface
     */
    private $api;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var ProductValidator
     */
    private $productValidator;

    /**
     * OrderCreatedSubscriber constructor.
     * @param ERPApiInterface $api
     * @param EntityManagerInterface $entityManager
     * @param \Swift_Mailer $mailer
     * @param ProductRepository $productRepository
     * @param ProductValidator $productValidator
     * @param string $from
     * @param string $to
     * @param string $subject
     */
    public function __construct(
        ERPApiInterface $api,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        ProductRepository $productRepository,
        ProductValidator $productValidator,
        string $from,
        string $to,
        string $subject
    ) {
        $this->api = $api;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->productValidator = $productValidator;
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            OrderCreatedEvent::NAME => [
                ['sendOrderToApi', 0],
                ['sendEmailToManager', 10]
            ]
        ];
    }

    /**
     * @param OrderCreatedEvent $event
     */
    public function sendEmailToManager(OrderCreatedEvent $event)
    {
        $cart = $event->getCart();

        if ($cart->getIsOrdered()) {
            $message = $this->getSwiftMessage($cart);
            $this->mailer->send($message);
        }
    }

    /**
     * @param OrderCreatedEvent $event
     * @throws \Exception
     */
    public function sendOrderToApi(OrderCreatedEvent $event)
    {
        $cart = $event->getCart();
        $user = $cart->getUser();
        /** @var CartDetail[]|Collection $cartDetails */
        $cartDetails = $cart->getCartDetails();
        $products = [];

        for ($i = 0; $i < $cartDetails->count(); $i++) {
           $products[$i]['ItemId'] = $cartDetails[$i]->getProductOuterId();
           $products[$i]['Quantity'] = $cartDetails[$i]->getAmount();
        }

        try {
            $this->productValidator->validateProducts($products);
        } catch (ProductValidatorException $exception) {
            $cart->setIsOrdered(false);
            $this->entityManager->flush();
        }

        $orderId = $cart->getId();
        $requiredShippingDate = (new \DateTime())
            ->modify('+1 day')
            ->format('Y-m-d');

        try {
            $this->api->createOrder(
                $user->getUsername(),
                $user->getPassword(),
                $orderId,
                $requiredShippingDate,
                $products
            );
        } catch (ERPApiException $exception) {
            $body = $exception->getMessage()
                . '; Method: ' . $exception->getMethod()
                . '; Uri: ' . $exception->getUri()
                . '; Params: ' . json_encode($exception->getParams());

            $message = new \Swift_Message();
            $message
                ->setSubject('Лог ошибки Giper LK')
                ->setBody($body, 'text/html')
                ->setFrom('euforie@ya.ru', 'Гипер ЛК')
                ->setTo('m0041k@gmail.com');

            $this->mailer->send($message);
        }
    }

    /**
     * @param Cart $cart
     *
     * @return \Swift_Message
     */
    private function getSwiftMessage(Cart $cart): \Swift_Message
    {
        $body = $this->getMessageBody($cart);
        $message = new \Swift_Message();
        $message
            ->setSubject($this->subject)
            ->setBody($body, 'text/html')
            ->setFrom($this->from, 'Гипер ЛК')
            ->setTo($this->to);

        return $message;
    }

    /**
     * @param Cart $cart
     *
     * @return string
     */
    private function getMessageBody(Cart $cart): string
    {
        $cartDetails = $cart->getCartDetails();
        $articles = [];
        $details = [];

        foreach ($cartDetails as $cartDetail) {
            $article = $cartDetail->getArticle();
            $articles[] = $article;
            $details[$article] = $cartDetail;
        }

        /** @var Product[] $products */
        $products = $this->productRepository->findBy([
            'article' => $articles
        ]);

        $username = $cart->getUser()->getUsername();
        $body = "Пользователем $username были заказаны следующие товары:<br/>";

        foreach ($products as $product) {
            /** @var CartDetail $currentCartDetail */
            $currentCartDetail = $details[$product->getArticle()];
            $body .= 'Название: ' . $product->getName() . '; ';
            $body .= 'Артикул: ' . $product->getArticle() . '; ';
            $body .= 'Стоимость: ' . $product->getPriceThree() . '; ';
            $body .= 'В количестве: ' . $currentCartDetail->getAmount() . '<br/></br>';
        }

        return $body;
    }
}