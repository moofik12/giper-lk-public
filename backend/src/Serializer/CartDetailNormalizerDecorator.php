<?php

namespace App\Serializer;

use App\Entity\CartDetail;
use App\Entity\Product;

final class CartDetailNormalizerDecorator extends PriceDependentNormalizerDecorator
{
    /**
     * {@inheritdoc}
     */
    public function decorate($data, $object, $format, $context): array
    {
        if ($object instanceof CartDetail) {
            $this->resolveAvailablePrices();
            /** @var Product $product */
            $product = $this->productRepository->findOneBy(['article' => $object->getArticle()]);

            if ($product) {
                $data['product'] = $this->getRealProductData($product);
            } else {
                $data['product'] = $this->getRememberedProductData($object);
            }

            $data = $this->purifyData($data);
        }

        return $data;
    }


    /**
     * @param Product $product
     *
     * @return array
     */
    private function getRealProductData(Product $product)
    {
        $data = [
            '@id' => '/api/products/' . $product->getId(),
            '@type' => 'Product'
        ];

        return array_merge($data, $product->serialize());
    }

    /**
     * @param CartDetail $cartDetail
     *
     * @return array
     */
    private function getRememberedProductData(CartDetail $cartDetail)
    {
        $data = [
            '@type' => 'Product',
            'article' => $cartDetail->getArticle(),
            'name' => $cartDetail->getName(),
            'retail_price' => $cartDetail->getRetailPrice(),
            'price_two' => $cartDetail->getPriceTwo(),
            'price_three' => $cartDetail->getPriceThree(),
            'description' => $cartDetail->getDescription(),
            'quantity' => $cartDetail->getQuantity(),
        ];

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function purifyData(array $data)
    {
        unset($data['article']);
        unset($data['name']);
        unset($data['retail_price']);
        unset($data['description']);
        unset($data['quantity']);

        if (!$this->price2available) {
            unset($data['price_two']);
        } elseif (!$this->price3available) {
            unset($data['price_three']);
        }

        return $data;
    }
}