<?php

namespace App\Serializer;

use App\Entity\User;
use App\Repository\ProductRepository;
use App\Service\ERP\ERPApiInterface;
use App\Service\ERP\Pricelist\PricelistResolver;
use App\Service\ERP\Pricelist\PricelistResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class PriceDependentNormalizerDecorator extends AbstractNormalizerDecorator
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var PricelistResolverInterface
     */
    protected $pricelistResolver;

    /**
     * @var bool
     */
    protected $price2available;

    /**
     * @var bool
     */
    protected $price3available;

    /**
     * @var ERPApiInterface
     */
    protected $ERPApi;

    /**
     * ProductNormalizerDecorator constructor.
     * @param NormalizerInterface $decorated
     * @param ProductRepository $productRepository
     * @param TokenStorageInterface $tokenStorage
     * @param PricelistResolverInterface $pricelistResolver
     * @param ERPApiInterface $ERPApi
     */
    public function __construct(
        NormalizerInterface $decorated,
        ProductRepository $productRepository,
        TokenStorageInterface $tokenStorage,
        PricelistResolverInterface $pricelistResolver,
        ERPApiInterface $ERPApi
    ) {
        parent::__construct($decorated);
        $this->productRepository = $productRepository;
        $this->tokenStorage = $tokenStorage;
        $this->pricelistResolver = $pricelistResolver;
        $this->ERPApi = $ERPApi;
    }

    protected function resolveAvailablePrices()
    {
        if (!isset($this->price2available) && !isset($this->price3available)) {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $username = $user->getUsername();
            $passwordHash = $user->getPassword();
            $availablePricelistDTO = $this->ERPApi->getAvailablePricelists($username, $passwordHash);

            $this->price2available = $this->pricelistResolver->checkIfPriceAvailable(
                PricelistResolver::PRICELIST_TYPE_2,
                $availablePricelistDTO
            );

            $this->price3available = $this->pricelistResolver->checkIfPriceAvailable(
                PricelistResolver::PRICELIST_TYPE_3,
                $availablePricelistDTO
            );
        }
    }
}