<?php

namespace App\Serializer;

use App\Entity\Product;

final class ProductNormalizerDecorator extends PriceDependentNormalizerDecorator
{
    /**
     * {@inheritdoc}
     */
    public function decorate($data, $object, $format, $context): array
    {
        if ($object instanceof Product) {
            $this->resolveAvailablePrices();

            if (!$this->price2available) {
                unset($data['price_two']);
            } elseif (!$this->price3available) {
                unset($data['price_three']);
            }
        }

        return $data;
    }
}