<?php

namespace App\DataFixtures;

final class ProductProvider
{
    private const PRODUCT_BRAND = [
        'Philips',
        'Braun',
        'DeLonghi',
        'Kenwood',
        'Panasonic',
        'Daewoo',
        'LG',
        'Sekonda',
        'Vitek',
        'Zanussi',
        'Hotpoint/Ariston',
        'Goldstar',
        'Bosch'
    ];

    /**
     * @return string
     */
    public static function brand()
    {
        return self::PRODUCT_BRAND[array_rand(self::PRODUCT_BRAND)];
    }
}