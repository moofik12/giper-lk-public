<?php

namespace App\DataFixtures;

final class CategoryProvider
{
    private const CATEGORY_ADJECTIVE = [
        'Железные',
        'Пластмассовые',
        'Керамические',
        'Прозрачные',
        'Стеклянные',
    ];

    private const CATEGORY_NOUN = [
        'Микроволновки',
        'Холодильники',
        'Телевизоры',
        'Пылесосы',
        'Фены',
    ];

    /**
     * @return string
     */
    public static function category()
    {
        return self::CATEGORY_ADJECTIVE[array_rand(self::CATEGORY_ADJECTIVE)]
            . ' '
            . self::CATEGORY_NOUN[array_rand(self::CATEGORY_NOUN)];
    }
}