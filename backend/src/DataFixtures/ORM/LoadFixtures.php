<?php

namespace App\DataFixtures\ORM;

use App\DataFixtures\CustomNativeLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $loader = new CustomNativeLoader();
        $objectSet = $loader->loadFile(__DIR__ . '/fixture.yaml')->getObjects();
        foreach($objectSet as $object) {
            $manager->persist($object);
        }
        $manager->flush();
    }
}