<?php

namespace App\Service\ERP;

use App\Service\ERP\Pricelist\AvailablePricelistDTO;

interface ERPApiInterface
{
    /**
     * Получает данные пользователя из ERP
     *
     * @param string $username
     * @param string $passwordHash
     *
     * @return array
     * @throws ERPApiException
     */
    public function getUser(string $username, string $passwordHash): array;

    /**
     * Получает список доступных прайс-листов для конкретного пользователя из ERP
     *
     * @param string $username
     * @param string $password
     * @return AvailablePricelistDTO
     */
    public function getAvailablePricelists(string $username, string $password): AvailablePricelistDTO;
}