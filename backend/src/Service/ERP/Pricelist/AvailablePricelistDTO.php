<?php

namespace App\Service\ERP\Pricelist;

class AvailablePricelistDTO
{
    /**
     * @var array
     */
    private $availablePricelists;

    /**
     * @var bool
     */
    private $hideRestrictedProduct;

    /**
     * @var bool
     */
    private $doNotIncludeInPricelist;

    /**
     * @return array
     */
    public function getAvailablePricelists(): array
    {
        return $this->availablePricelists;
    }

    /**
     * @param array $availablePricelists
     */
    public function setAvailablePricelists(array $availablePricelists): void
    {
        $this->availablePricelists = $availablePricelists;
    }

    /**
     * @return bool
     */
    public function isHideRestrictedProduct(): bool
    {
        return $this->hideRestrictedProduct;
    }

    /**
     * @param bool $hideRestrictedProduct
     */
    public function setHideRestrictedProduct(bool $hideRestrictedProduct): void
    {
        $this->hideRestrictedProduct = $hideRestrictedProduct;
    }

    /**
     * @return bool
     */
    public function isDoNotIncludeInPricelist(): bool
    {
        return $this->doNotIncludeInPricelist;
    }

    /**
     * @param bool $doNotIncludeInPricelist
     */
    public function setDoNotIncludeInPricelist(bool $doNotIncludeInPricelist): void
    {
        $this->doNotIncludeInPricelist = $doNotIncludeInPricelist;
    }
}