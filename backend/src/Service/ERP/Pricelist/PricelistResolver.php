<?php

namespace App\Service\ERP\Pricelist;

class PricelistResolver implements PricelistResolverInterface
{
    public const PRICELIST_TYPE_3 = 'ПРАЙС 3';
    public const PRICELIST_TYPE_2 = 'ПРАЙС 2';
    public const PRICELIST_TYPE_RETAIL = 'РРЦ';

    private const PATH_PRICELIST_MAIN = '/var/storage/pricelist.xml';
    private const PATH_PRICELIST_2 = '/var/storage/price3.xml';
    private const PATH_PRICELIST_3 = '/var/storage/price3.xml';

    /**
     * @var string
     */
    private $rootDir;

    /**
     * PricelistResolver constructor.
     * @param string $rootDir
     */
    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @param AvailablePricelistDTO $availablePricelistDTO
     *
     * @return string|null
     */
    public function getPricelistFilePath(AvailablePricelistDTO $availablePricelistDTO): ?string
    {
        $availablePricelists = $availablePricelistDTO->getAvailablePricelists();
        $availablePricelists = array_column($availablePricelists, 'PriceName');

        $price3Available = in_array(self::PRICELIST_TYPE_3, $availablePricelists);
        $price2Available = in_array(self::PRICELIST_TYPE_2, $availablePricelists);

        if ($price2Available && $price3Available) {
            return $this->rootDir . self::PATH_PRICELIST_MAIN;
        } elseif ($price2Available) {
            return $this->rootDir . self::PATH_PRICELIST_2;
        } elseif ($price3Available) {
            return $this->rootDir . self::PATH_PRICELIST_3;
        }

        return null;
    }

    /**
     * @param string $type
     * @param AvailablePricelistDTO $availablePricelistDTO
     *
     * @return bool
     */
    public function checkIfPriceAvailable(string $type, AvailablePricelistDTO $availablePricelistDTO): bool
    {
        $availablePricelists = $availablePricelistDTO->getAvailablePricelists();
        $availablePricelists = array_column($availablePricelists, 'PriceName');

        if (in_array($type, $availablePricelists)) {
            return true;
        }

        return false;
    }
}