<?php

namespace App\Service\ERP\Pricelist;

interface PricelistResolverInterface
{
    public function getPricelistFilePath(AvailablePricelistDTO $availablePricelistDTO): ?string;

    public function checkIfPriceAvailable(string $type, AvailablePricelistDTO $availablePricelistDTO): bool;
}