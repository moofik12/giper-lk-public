<?php

namespace App\Service\ERP\Api;

use App\Service\ERP\ERPApiException;
use App\Service\ERP\ERPApiInterface;
use App\Service\ERP\Pricelist\AvailablePricelistDTO;
use Psr\Log\LoggerInterface;

class DummyApi implements ERPApiInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DummyApi constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(string $username, string $passwordHash): array
    {
        $dummyPasswordHash = hash('sha256', 'test');

        if ('johndoe' !== $username || $dummyPasswordHash !== $passwordHash) {
            throw new ERPApiException('Bad credentials');
        }

        return [
            [
                'UserID' => 1,
                'UserName' => 'johndoe',
                'Gender' => 'Test',
                'Position' => 'Test',
            ]
        ];
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return AvailablePricelistDTO
     */
    public function getAvailablePricelists(string $username, string $password): AvailablePricelistDTO
    {
        $response = [
            [
                'AvailablePriceLists' => [
                    [
                        'PriceID' => '000bd8d6-29fe-11e3-b2d9-002590a746c1',
                        'PriceName' => 'РРЦ',
                    ],
                    [
                        'PriceID' => '0cc8cffc-29fe-11e3-b2d9-002590a746c1',
                        'PriceName' => 'ПРАЙС 2',
                    ],
                ],
            ],
            [
                'HideRestrictedProduct' => 1,
            ],
            [
                'DoNotIncludeInPriceList' => 0,
            ],
        ];

        $availablePricelistsDto = new AvailablePricelistDTO();
        $availablePricelistsDto->setAvailablePricelists($response[0]['AvailablePriceLists']);
        $availablePricelistsDto->setHideRestrictedProduct((bool)$response[1]['HideRestrictedProduct']);
        $availablePricelistsDto->setHideRestrictedProduct((bool)$response[2]['DoNotIncludeInPriceList']);

        return $availablePricelistsDto;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $username
     * @param string $passwordHash
     * @param int $orderId
     * @param string $shippingDate
     * @param array $products
     */
    public function createOrder(
        string $username,
        string $passwordHash,
        int $orderId,
        string $shippingDate,
        array $products
    ): void {
        $this->logger->critical('ERP Dummy API: Order has been created');
    }
}
