<?php

namespace App\Service\ERP\Api;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class GiperApiFactory
{
    private const BASE_TEST_URL = 'MOCK_LINK_1';
    private const BASE_PROD_URL = 'MOCK_LINK_2';

    /**
     * @param LoggerInterface $logger
     * @param bool $test
     *
     * @return GiperApi
     */
    public static function createGiperApi(
        LoggerInterface $logger,
        bool $test
    ): GiperApi {
        $userAgent = 'Mozilla/5.0 (Linux x86_64) GuzzleHttp/' . Client::VERSION;

        $client = new Client([
            'base_uri' => $test ? static::BASE_PROD_URL : static::BASE_PROD_URL,
            'headers' => ['User-Agent' => $userAgent],
            'auth' => ['test', 'test'],
        ]);

        return new GiperApi($client, $logger);
    }
}
