<?php

namespace App\Service\ERP\Api;

use App\Service\ERP\ERPApiException;
use App\Service\ERP\ERPApiInterface;
use App\Service\ERP\Pricelist\AvailablePricelistDTO;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Psr\Log\LoggerInterface;

class GiperApi implements ERPApiInterface
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     */
    public function __construct(ClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(string $username, string $passwordHash): array
    {
        $uri = 'Authorize/' . $username . '/' . $passwordHash;
        $response = $this->call('GET', $uri);

        if (!is_array($response)) {
            throw $this->makeError('Authorization response data is invalid.', 'GET', '/authorize', $response);
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailablePricelists(string $username, string $passwordHash): AvailablePricelistDTO
    {
        $uri = 'AvailablePriceLists/' . $username . '/' . $passwordHash;
        $response = $this->call('GET', $uri);

        $availablePricelistsDto = new AvailablePricelistDTO();
        $availablePricelistsDto->setAvailablePricelists($response[0]['AvailablePriceLists']);
        $availablePricelistsDto->setHideRestrictedProduct((bool)$response[1]['HideRestrictedProduct']);
        $availablePricelistsDto->setHideRestrictedProduct((bool)$response[2]['DoNotIncludeInPriceList']);

        return $availablePricelistsDto;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $username
     * @param string $passwordHash
     * @param int $orderId
     * @param string $shippingDate
     * @param array $products
     */
    public function createOrder(
        string $username,
        string $passwordHash,
        int $orderId,
        string $shippingDate,
        array $products
    ): void {
        $uri = 'CreateOrder/' . $username . '/' . $passwordHash;

        $json = [
            'Order' => [
                'OrderNum' => $orderId,
                'ShippingDate' => $shippingDate,
                'Lines' => $products
            ]
        ];

        $this->call('POST', $uri, ['json' => $json]);
    }


    /**
     * @param string $method
     * @param string $uri
     * @param array|null $params
     *
     * @return array
     * @throws ERPApiException
     */
    private function call(string $method, string $uri, array $params = []): array
    {
        try {
            $this->logger->info(sprintf('Sending %s request to Giper ERP: %s with json %s.', $method, $uri, json_encode($params)));

            $response = $this->client->request($method, $uri, $params);
            $response = json_decode($response->getBody(), true);

            $this->logger->debug(sprintf('Received response from Giper ERP API'), ['response' => $response]);
        } catch (GuzzleException $e) {
            $errorDetails = '';

            if ($e instanceof RequestException || $e instanceof ServerException) {
                $errorDetails = $e
                    ->getResponse()
                    ->getBody()
                    ->getContents();

                if (is_array($errorDetails)) {
                    $errorDetails = json_encode($errorDetails);
                }
            }

            $error = sprintf('Body: %s.', $errorDetails);

            throw new ERPApiException(
                sprintf('Giper ERP API error: %s.', $error), $method, $uri, $params,null, $e
            );
        }

        if (!isset($response['Status']) || !isset($response['Response'])) {
            throw $this->makeError('Invalid response data', $method, $uri, $response);
        }

        return $response['Response'];
    }

    /**
     * @param string $message
     * @param string|null $method
     * @param string|null $uri
     * @param array|null $params
     * @param array|null $response
     *
     * @return ERPApiException
     */
    private function makeError(
        string $message,
        string $method = null,
        string $uri = null,
        array $params = null,
        array $response = null
    ): ERPApiException {
        $this->logger->error('Giper ERP API problem: ' . $message, [
            'method' => $method,
            'uri' => $uri,
            'response' => $response,
        ]);

        return new ERPApiException($message, $method, $uri, $params, $response);
    }
}