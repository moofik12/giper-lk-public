<?php

namespace App\Service\ERP\Product;

class ProductValidator
{
    /**
     * @param array $product
     *
     * @throws ProductValidatorException
     */
    public function validateProduct(array $product)
    {
        if (!isset($product['ItemId'])) {
            throw new ProductValidatorException('ERP Product validation error: ItemId field required.');
        }

        if (!isset($product['Quantity'])) {
            throw new ProductValidatorException('ERP Product validation error: Quantity field required.');
        }

        if (!isset($product['Price']) && count($product) > 2) {
            throw new ProductValidatorException('ERP Product validation error: unprocessable fields.');
        }
    }

    /**
     * @param array $products
     *
     * @throws ProductValidatorException
     */
    public function validateProducts(array $products)
    {
        foreach ($products as $product) {
            if (!is_array($product)) {
                throw new ProductValidatorException('ERP Product must be represented as array.');
            }

            $this->validateProduct($product);
        }
    }
}