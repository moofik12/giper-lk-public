<?php

namespace App\Service\ERP;

use Throwable;

class ERPApiException extends \Exception
{
    /**
     * @var string|null
     */
    private $method;

    /**
     * @var string|null
     */
    private $uri;

    /**
     * @var string[]|null
     */
    private $params;

    /**
     * @var array|null
     */
    private $response;

    /**
     * ApiException constructor.
     *
     * @param string $message
     * @param string|null $method
     * @param string|null $uri
     * @param string[]|null $params
     * @param array|null $response
     * @param Throwable|null $previous
     */
    public function  __construct(
        string $message,
        string $method = null,
        string $uri = null,
        array $params = null,
        array $response = null,
        Throwable $previous = null
    ) {
        $this->method = $method;
        $this->uri = $uri;
        $this->params = $params;
        $this->response = $response;

        parent::__construct($message, 0, $previous);
    }

    /**
     * @return null|string
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @return null|string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @return null|string[]
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @return array|null
     */
    public function getResponse(): ?array
    {
        return $this->response;
    }
}
