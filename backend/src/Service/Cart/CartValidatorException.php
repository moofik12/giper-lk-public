<?php

namespace App\Service\Cart;

use App\Entity\CartDetail;
use App\Entity\Product;
use Throwable;

class CartValidatorException extends CartException
{
    /**
     * @var array
     */
    private $nonexistentDetails;

    public function __construct(
        string $message = "",
        array $nonexistentDetails = [],
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->nonexistentDetails = $nonexistentDetails;
    }

    /**
     * @return CartDetail[]
     */
    public function getNonexistentDetails(): array
    {
        return $this->nonexistentDetails;
    }
}