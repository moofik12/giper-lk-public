<?php

namespace App\Service\Cart;

use App\Entity\Cart;
use App\Entity\User;

interface CartServiceInterface
{
    /**
     * @param User $user
     * @param array $cartProducts
     *
     * @throws CartServiceException
     */
    public function createCart(User $user, array $cartProducts);

    /**
     * @param Cart $cart
     * @param array $cartProducts
     *
     * @throws CartServiceException
     */
    public function updateCart(Cart $cart, array $cartProducts);
}