<?php

namespace App\Service\Cart;

use App\Entity\Cart;
use App\Entity\CartDetail;
use App\Entity\Product;
use App\Repository\ProductRepository;

class CartValidator implements CartValidatorInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CartValidator constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function validateCartProductData(array $data): void
    {
        if (!isset($data['article']) || !isset($data['quantity'])) {
            throw new CartValidatorException('Некорректные данные продукта');
        }

        /** @var Product $product */
        $product = $this->productRepository->findOneBy([
            'article' => $data['article']
        ]);

        if (!$product) {
            throw new CartValidatorException('Товара с таким артикулом не существует');
        }

        if (!$product->getActuality()) {
            throw new CartValidatorException('Вы пытаетесь добавить в корзину отсутствующий товар: ' . $product->getName());
        }

        if ($product->getQuantity() < $data['quantity']) {
            throw new CartValidatorException("Товара с артикулом {$product->getArticle()} нет на складе в нужном количестве");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateWithRelations(Cart $cart): void
    {
        /** @var CartDetail[] $cartDetails */
        $cartDetails = $cart->getCartDetails();
        $productArticles = [];

        foreach ($cartDetails as $cartDetail) {
            $productArticles[] = $cartDetail->getArticle();
        }

        $existentArticles = $this->productRepository->findExistentArticles($productArticles);

        if (count($productArticles) > count($existentArticles)) {
            $nonexistentDetails = [];

            foreach ($cartDetails as $cartDetail) {
                if (!in_array($cartDetail->getArticle(), $existentArticles)) {
                    $nonexistentDetails[] = $cartDetail;
                }
            }

            throw new CartValidatorException(
                'Данная корзина ссылается на уже несуществующие товары',
                $nonexistentDetails
            );
        }
    }
}