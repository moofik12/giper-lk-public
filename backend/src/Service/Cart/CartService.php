<?php

namespace App\Service\Cart;

use App\Entity\Cart;
use App\Entity\CartDetail;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

class CartService implements CartServiceInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var CartValidatorInterface
     */
    private $validator;

    /**
     * CartService constructor.
     * @param EntityManagerInterface $entityManager
     * @param CartValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CartValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->productRepository = $entityManager->getRepository(Product::class);
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     *
     * @throws CartValidatorException
     */
    public function createCart(User $user, array $cartProducts)
    {
        $cart = new Cart();
        $cart->setUser($user);
        $this->entityManager->persist($cart);

        foreach ($cartProducts as $cartProduct) {
            $this->validator->validateCartProductData($cartProduct);
            $this->saveCartDetail($cart, $cartProduct);
        }

        $this->entityManager->flush();

        return $cart;
    }

    /**
     * {@inheritdoc}
     *
     * @throws CartValidatorException
     */
    public function updateCart(Cart $cart, array $cartProducts)
    {
        if (0 === count($cartProducts)) {
            $this->entityManager->remove($cart);
            $this->entityManager->flush();

            return;
        }

        /** @var CartDetail[]|Collection $existentProducts */
        $existentProducts = $cart->getCartDetails();
        $updatedProductArticles = array_column($cartProducts, 'article');
        $existentProductArticles = $existentProducts
            ->map(function ($obj) {
                /** @var CartDetail $obj */
                return $obj->getArticle();
            })
            ->getValues();

        foreach ($existentProducts as $existentProduct) {
            $existentProductArticle = $existentProduct->getArticle();

            foreach ($cartProducts as $index => $cartProduct) {
                /** Проверяем, существует ли на текущий момент товар с переданным id в корзине */
                if ($existentProductArticle === $cartProduct['article']) {
                    if (0 === $cartProduct['quantity']) {
                        $this->entityManager->remove($existentProduct);
                    } else {
                        $existentProduct->setAmount($cartProduct['quantity']);
                    }
                } elseif (!in_array($cartProduct['article'], $existentProductArticles)) {
                    $this->validator->validateCartProductData($cartProduct);
                    $this->saveCartDetail($cart, $cartProduct);
                    unset($cartProducts[$index]);
                }
            }

            if (!in_array($existentProductArticle, $updatedProductArticles)) {
                $this->entityManager->remove($existentProduct);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param Cart $cart
     * @param array $cartProduct
     *
     * @throws CartServiceException
     */
    private function saveCartDetail(Cart $cart, array $cartProduct)
    {
        /** @var Product $product */
        $product = $this->productRepository->findOneBy([
            'article' => $cartProduct['article']
        ]);

        $cartDetail = new CartDetail();
        $cartDetail->setArticle($product->getArticle());
        $cartDetail->setQuantity($product->getQuantity());
        $cartDetail->setPriceTwo($product->getPriceTwo());
        $cartDetail->setPriceThree($product->getPriceThree());
        $cartDetail->setName($product->getName());
        $cartDetail->setRetailPrice($product->getRetailPrice());
        $cartDetail->setDescription($product->getDescription());
        $cartDetail->setAmount($cartProduct['quantity']);
        $cartDetail->setCart($cart);
        $cartDetail->setIsExistent(true);
        $cartDetail->setProductOuterId($product->getProductOuterId());
        $this->entityManager->persist($cartDetail);
    }
}