<?php

namespace App\Service\Cart;

use App\Entity\Cart;

interface CartValidatorInterface
{
    /**
     * @param Cart $cart
     *
     * @throws CartValidatorException
     * @return void
     */
    public function validateWithRelations(Cart $cart): void;

    /**
     * @param array $data
     *
     * @throws CartValidatorException
     * @return void
     */
    public function validateCartProductData(array $data): void;
}