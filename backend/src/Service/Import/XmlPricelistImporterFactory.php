<?php

namespace App\Service\Import;

use Doctrine\ORM\EntityManagerInterface;

class XmlPricelistImporterFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * XmlPricelistImporterFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return XmlPricelistImporter
     */
    public function create(): XmlPricelistImporter
    {
        /*
         * Соответствие свойств из XML-прайслиста полям в таблице
         * Соответствие задается для каждого основного тэга (сущность импортируемая в БД - category, product)
         */
        $propertyToColumnMapping = [
            'category' => [
                'id' => 'category_outer_id',
                'innerHTML' => 'name',
                'ParentId' => 'parent_outer_id',
            ],
            'product' => [
                'id' => 'product_outer_id',
                'priceRetail' => 'retail_price',
                'Price2' => 'price_two',
                'Price3' => 'price_three',
                'categoryId' => 'category_outer_id',
                'Description' => 'description',
                'barcode' => 'barcode',
                'article' => 'article',
                'ItemVolume' => 'value',
                'ItemWeight' => 'weight',
                'BrandName' => 'brand',
                'productName' => 'name',
                'Actuality' => 'actuality',
                'RestrictedAccess' => 'restricted_access',
                'PackQty' => 'package_quantity',
                'quantity' => 'quantity',
            ],
        ];
        /* Соответствие вставляемых сущностей из XML файла на таблицы БД */
        $tagToTableMapping = [
            'category' => 'categories_tmp',
            'product' => 'products_tmp'
        ];
        /*
         * Соответствие тэгов которые мы хотим спарсить и тэгов сигнализирующих о необходимости остановки парсинга данных по этому тэгу
         * (Происходит переход к следующему тэгу)
         */
        $tagToStopTagMapping = [
            'category' => 'offers',
            'product' => 'yml_catalog'
        ];
        /*
         * Список свойств которые нужно заэкранировать при вставке в бд для каждого тэга
         */
        $propertyQuoteMapping = [
            'product' => [
                'name',
                'product_outer_id',
                'category_outer_id',
                'description',
                'barcode',
                'article',
                'brand',
            ]
        ];

        return new XmlPricelistImporter(
            $this->entityManager,
            $propertyToColumnMapping,
            $tagToTableMapping,
            $tagToStopTagMapping,
            $propertyQuoteMapping
        );
    }
}