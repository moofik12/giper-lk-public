<?php

namespace App\Service\Import;

use Generator;
use XMLReader;

class XmlPricelistParser
{
    public const TAG_CATEGORY = 'category';
    public const TAG_PRODUCT = 'product';

    private const STATE_LOOP = 0;
    private const STATE_EXTRACT = 1;

    /**
     * @var int
     */
    protected $state;

    /**
     * @var XMLReader
     */
    protected $reader;

    /**
     * @var Generator
     */
    protected $currentGenerator;

    /**
     * XmlPricelistParser constructor.
     *
     * @param XMLReader $reader
     * @param string $uri
     */
    public function __construct(XMLReader $reader, string $uri)
    {
        $this->reader = $reader;
        $this->reader->open($uri);
    }

    /**
     * @param string $tag
     * @param string $stopTag
     *
     *  @return array
     */
    public function getPropertyDataByTag(string $tag, string $stopTag)
    {
        switch ($tag) {
            case 'category':
                return $this->getCategoryPropertyData($stopTag);
            case 'product':
                return $this->getProductPropertyData($stopTag);
            default:
                return [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductPropertyData(string $stopTag): array
    {
        /** @var Generator $readerGenerator */
        $readerGenerator = $this->getNodeReader('offer', $stopTag);
        /** @var XMLReader $xmlTagReader */
        $xmlTagReader = $readerGenerator->current();
        if (!$xmlTagReader) {
            return [];
        }

        $xmlTag = $xmlTagReader->readOuterXml();
        $readerGenerator->next();

        preg_match('/<.*?quantity="(.*?)">/', $xmlTag, $quantityMatch);
        preg_match('/<offer id="(.*?)"/', $xmlTag, $idMatch);
        preg_match_all('/<.*?code="(.*?)">(.*?)<\/.*?>/', $xmlTag, $dataTags);
        preg_match_all('/<([^=\/]*?)>(.*?)<\/.*?>/', $xmlTag, $dataAttributes);

        $id = ['id' => $idMatch[1]];
        $quantity = ['quantity' => $quantityMatch[1]];
        $resultTags = array_combine($dataTags[1], $dataTags[2]);
        $resultAttributes = array_combine($dataAttributes[1], $dataAttributes[2]);
        $result = array_merge($resultTags, $resultAttributes, $quantity, $id);

        foreach ($result as $key => $value) {
            $result[$key] = str_replace(["'", '"'], '', $value);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getCategoryPropertyData(string $stopTag): array
    {
        $propertyData = $this->getPropertyDataByAttribute('category', $stopTag);

        return $propertyData;
    }

    /**
     * Формирует массив с инфо о xml тэге на основании его аттрибутов
     *
     * @param string $tag - имя xml тэга в документе
     * @param string $stopTag
     *
     * @return array - результат работы preg_match
     */
    protected function getPropertyDataByAttribute(string $tag, string $stopTag)
    {
        /** @var Generator $readerGenerator */
        $readerGenerator = $this->getNodeReader($tag, $stopTag);
        /** @var XMLReader $xmlTagReader */
        $xmlTagReader = $readerGenerator->current();
        if (!$xmlTagReader) {
            return [];
        }

        $result = [];
        $attributeCount = $xmlTagReader->attributeCount;
        for ($i = 0; $i < $attributeCount; $i++) {
            $xmlTagReader->moveToNextAttribute();
            $result[$xmlTagReader->name] = "'" . $xmlTagReader->value . "'";
        }

        if (!empty($result)) {
            $result['innerHTML'] = "'" . $this->reader->readInnerXml() . "'";
        }

        $readerGenerator->next();

        return $result;
    }

    /**
     * @param string $tag
     * @param string $stopTag
     *
     * @return Generator
     */
    protected function getNodeReader(string $tag, string $stopTag): Generator
    {
        if (!$this->currentGenerator) {
            $this->state = self::STATE_LOOP;
            $this->currentGenerator = $this->getTagContentGenerator($tag, $stopTag);
        }

        yield $this->currentGenerator->current();

        if ($this->currentGenerator) {
            $this->currentGenerator->next();
        }
    }

    /**
     * @param string $tag
     * @param string $stopTag
     *
     * @return Generator
     */
    private function getTagContentGenerator(string $tag, string $stopTag)
    {
        $minDepth = 0;
        if (self::STATE_LOOP === $this->state) {
            /* Перемещаем итератор к тэгу $tag tag*/
            while ($this->reader->name !== $tag) {
                $readSuccess = $this->reader->name !== $stopTag && $this->reader->read();
                if (!$readSuccess) {
                    $this->currentGenerator = null;
                    yield false;
                    break;
                }
            }
            /* Переключаемся на режим извлечения информации из нужного нам тэга */
            $this->state = self::STATE_EXTRACT;
            $minDepth = $this->reader->depth;
        }

        $closingTag = true;
        while (self::STATE_EXTRACT === $this->state) {
            if ($this->reader->depth < $minDepth) {
                $this->state = self::STATE_LOOP;
                $this->currentGenerator = null;
                break;
            }

            if ($tag === $this->reader->name) {
                /* Встретили закрывающийся тэг - пропускаем его */
                $closingTag = !$closingTag;
                if ($closingTag) {
                    $this->reader->read();
                    continue;
                }

                yield $this->reader;
            }
            /* Двигаемся к следующему тэгу */
            $this->reader->read();
        }
    }
}
