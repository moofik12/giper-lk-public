<?php

namespace App\Service\Import;

use XMLReader;

class XmlPricelistParserFactory
{
    /**
     * @param $uri
     *
     * @return XmlPricelistParser
     */
    public function create($uri)
    {
        return new XmlPricelistParser(new XMLReader(), $uri);
    }
}