<?php

namespace App\Service\Import;

use App\Entity\Category;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;

class XmlPricelistImporter
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array
     */
    private $propertyToColumnMapping;

    /**
     * @var array
     */
    private $propertyQuoteMapping;

    /**
     * @var array
     */
    private $tagToTableMapping;

    /**
     * @var array
     */
    private $stopTagToTagMapping;

    /**
     * XmlPricelistImporter constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param array $propertyToColumnMapping
     * @param array $tagToTableMapping
     * @param array $stopTagToTagMapping
     * @param array $propertyQuoteMapping
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        array $propertyToColumnMapping,
        array $tagToTableMapping,
        array $stopTagToTagMapping,
        array $propertyQuoteMapping
    )
    {
        $this->entityManager = $entityManager;
        $this->propertyToColumnMapping = $propertyToColumnMapping;
        $this->tagToTableMapping = $tagToTableMapping;
        $this->stopTagToTagMapping = $stopTagToTagMapping;
        $this->propertyQuoteMapping = $propertyQuoteMapping;
    }

    /**
     * Запускает импорт из файла
     *
     * @param XmlPricelistParser $parser - парсер с открытым потоком чтения файла
     * @param int $chunkSize - количество операций за один запрос в БД
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function importFromFile(XmlPricelistParser $parser, int $chunkSize)
    {
        $this->clearTmpTables();
        $this->createRootCategory();

        foreach ($this->tagToTableMapping as $tag => $table) {
            do {
                $chunkQuery = $this->getQueryForChunk(
                    $parser,
                    $tag,
                    $this->stopTagToTagMapping[$tag],
                    $table,
                    $chunkSize
                );

                if ('' !== $chunkQuery) {
                    $this->entityManager
                        ->getConnection()
                        ->exec($chunkQuery);
                }
            } while ('' !== $chunkQuery);
        }

        $this->bindProductsToCategories();
        $this->bindParentIdToCategories();
        $this->bindRootCategory();
        $this->fillPivotTable();
        $this->switchActualTables();
    }

    /**
     * Очищает временные таблицы для импорта категорий и продуктов из прайслиста
     *
     * @throws DBALException
     */
    private function clearTmpTables()
    {
        $query = <<<SQL
DELETE FROM category_product_tmp;
DELETE FROM products_tmp;
DELETE FROM categories_tmp;
SQL;
        $this->entityManager
            ->getConnection()
            ->exec($query);
    }

    /**
     * Генерирует SQL запрос для вставки нужного количества записей в БД
     *
     * @param XmlPricelistParser $parser
     * @param string $tag сущность для которой генерируется запрос (product, category)
     * @param string $stopTag стоп-тэг для сущности
     * @param string $table таблица в которую будут вставляться записи для этой сущности
     * @param int $size количество сгенерированных записей для импорта за одну операцию вставки
     *
     * @return string SQL запрос с нужным количеством записей для вставки в таблицу
     */
    private function getQueryForChunk(XmlPricelistParser $parser, string $tag, string $stopTag, string $table, int $size)
    {
        $sql = '';

        for ($i = 0; $i < $size; ++$i) {
            $propertyData = $parser->getPropertyDataByTag($tag, $stopTag);
            if (empty($propertyData)) {
                return $sql;
            }

            $propertyData = $this->filterParametersWithMapping($tag, $this->propertyToColumnMapping, $propertyData);
            $propertyData = $this->quoteParametersWithMapping($tag, $this->propertyQuoteMapping, $propertyData);

            $sql .= $this->getInsertSQLQuery($table, $propertyData);
        }

        return $sql;
    }

    /**
     * Отсеивает заданные в маппинге свойства сущности из данных для SQL запроса
     * (для которых нет соответствующих столбцов в БД)
     *
     * @param string $tag идентификатор сущности для которой запускается фильтрация
     * @param array $mapping
     * @param array $propertyData данные для SQL запроса
     *
     * @return array отфильтрованный массив с данными для SQL запроса
     */
    private function filterParametersWithMapping(string $tag, array $mapping, array $propertyData)
    {
        $result = [];
        foreach ($mapping[$tag] as $key => $value) {
            if (isset($propertyData[$key])) {
                $result[$value] = $propertyData[$key];
            }
        }

        return $result;
    }

    /**
     * Экранирует заданные в маппинге параметры в данных для построения запроса
     *
     * @param string $tag идентификатор сущности для которой запускается фильтрация
     * @param array $mapping
     * @param array $propertyData данные для SQL запроса
     *
     * @return array массив с экранированными(там где задано) данными для SQL запроса
     */
    private function quoteParametersWithMapping(string $tag, array $mapping, array $propertyData)
    {
        if (!isset($mapping[$tag])) {
            return $propertyData;
        }

        foreach ($mapping[$tag] as $key => $value) {
            if (isset($propertyData[$value])) {
                $propertyData[$value] = "'" . $propertyData[$value] . "'";
            }
        }

        return $propertyData;
    }

    /**
     * Возвращает SQL запрос для вставки в БД
     *
     * @param $table - таблица
     * @param $propertyData - данные для запроса
     *
     * @return string - SQL запрос
     */
    private function getInsertSQLQuery($table, $propertyData): string
    {
        $sql = 'INSERT INTO ' . $table;
        $sql .= ' (' . implode(',', array_keys($propertyData)) . ') ';
        $sql .= 'VALUES (' . implode(',', array_values($propertyData)) . ');';

        return $sql;
    }

    /**
     * Привязывает товары к категориям
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function bindProductsToCategories()
    {
        $query = <<<SQL
UPDATE products_tmp
SET category_id = categories_tmp.id 
FROM categories_tmp 
WHERE categories_tmp.category_outer_id = products_tmp.category_outer_id;
SQL;
        $this->entityManager
            ->getConnection()
            ->exec($query);
    }

    /**
     * Заполняет поле parent_id для категорий
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function bindParentIdToCategories()
    {
        $query = <<<SQL
UPDATE categories_tmp c 
SET parent_id = c2.id 
FROM categories_tmp c2 
WHERE c.parent_outer_id = c2.category_outer_id;
SQL;
        $this->entityManager
            ->getConnection()
            ->exec($query);
    }

    /**
     * Удаляет старые таблицы, подставляя на их место свежие заимпортированые таблицы
     *
     * @throws DBALException
     */
    private function switchActualTables()
    {
        $swapTablesQuery = <<<SQL
BEGIN;
ALTER TABLE "categories" RENAME TO "categories_old";
ALTER TABLE "categories_tmp" RENAME TO "categories";
ALTER TABLE "categories_old" RENAME TO "categories_tmp";
ALTER TABLE "products" RENAME TO "products_old";
ALTER TABLE "products_tmp" RENAME TO "products";
ALTER TABLE "products_old" RENAME TO "products_tmp";
ALTER TABLE "category_product" RENAME TO "category_product_old";
ALTER TABLE "category_product_tmp" RENAME TO "category_product";
ALTER TABLE "category_product_old" RENAME TO "category_product_tmp";
COMMIT;
SQL;

        $this->entityManager
            ->getConnection()
            ->exec($swapTablesQuery);
    }

    /**
     * Заполняет many-to-many таблицу category_product
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function fillPivotTable()
    {
        $getCategoriesIdsQuery = "SELECT id FROM categories_tmp";

        /** @noinspection PhpUnhandledExceptionInspection */
        $statement = $this->entityManager
            ->getConnection()
            ->prepare($getCategoriesIdsQuery);
        $statement->execute();
        /** @var array $categoriesIds */
        $categoriesIds = $statement->fetchAll(FetchMode::COLUMN);

        /** @var Category $category */
        foreach ($categoriesIds as $categoryId) {
            $getProductsIdsForCategoryQuery = "SELECT id FROM products_tmp WHERE category_id = :categoryId";

            /** @noinspection PhpUnhandledExceptionInspection */
            $statement = $this->entityManager
                ->getConnection()
                ->prepare($getProductsIdsForCategoryQuery);
            $statement->bindValue(':categoryId', $categoryId);
            $statement->execute();
            /** @var array $productIdsForCategory */
            $productIdsForCategory = $statement->fetchAll(FetchMode::COLUMN);

            /* Рекурсивный запрос чтобы для заданной категории достать из БД собственный id и id всех родительских категорий */
            $getCategoryAndItsParentsIdsQuery = <<<SQL
WITH RECURSIVE r AS (
  SELECT id, parent_id
  FROM categories_tmp
  WHERE id = :categoryId
  UNION
  SELECT categories_tmp.id, categories_tmp.parent_id
  FROM categories_tmp
  JOIN r ON r.parent_id = categories_tmp.id
)
SELECT id FROM r;
SQL;

            /** @noinspection PhpUnhandledExceptionInspection */
            $statement = $this->entityManager
                ->getConnection()
                ->prepare($getCategoryAndItsParentsIdsQuery);
            $statement->bindValue(':categoryId', $categoryId);
            $statement->execute();
            /** @var array $categoryAndItsParentsIds */
            $categoryAndItsParentsIds = $statement->fetchAll(FetchMode::COLUMN);

            $insertChunkIntoPivotQuery = "";
            /* В цикле ниже для каждого продукта подготовливает записи для вставки id каждого продукта + всех его родительских категорий */
            foreach ($productIdsForCategory as $productId) {
                foreach ($categoryAndItsParentsIds as $categoryNestedId) {
                    $insertChunkIntoPivotQuery .= "INSERT INTO category_product_tmp VALUES(";
                    $insertChunkIntoPivotQuery .= $categoryNestedId . ',' . $productId . ");";
                }
            }

            if ("" === $insertChunkIntoPivotQuery) {
                continue;
            }

            /** @noinspection PhpUnhandledExceptionInspection */
            $this->entityManager
                ->getConnection()
                ->exec($insertChunkIntoPivotQuery);
        }
    }

    /**
     * @throws DBALException
     */
    private function createRootCategory()
    {
        $sql = "INSERT INTO categories_tmp (parent_id, name) VALUES (null, 'Каталог')";
        $this->entityManager
            ->getConnection()
            ->exec($sql);
    }

    /**
     * @throws DBALException
     */
    private function bindRootCategory()
    {
        $sqlSelect = "SELECT id FROM categories_tmp WHERE name = 'Каталог'";

        /** @noinspection PhpUnhandledExceptionInspection */
        $statement = $this->entityManager
            ->getConnection()
            ->executeQuery($sqlSelect);

        /** @var array $categoriesIds */
        $rootId = $statement->fetchColumn();

        $sqlUpdate = <<<SQL
UPDATE categories_tmp SET parent_id = :rootId WHERE parent_id IS NULL AND id <> :rootId
SQL;
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->entityManager
            ->getConnection()
            ->executeUpdate($sqlUpdate, [
                ':rootId' => $rootId
            ]);
    }
}