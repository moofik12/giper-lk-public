<?php

namespace App\Service\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class SaveCartRequest implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="Тип значения {{ value }} должен быть {{ type }}.")
     */
    private $name;

    /**
     * SaveCartRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->name = $request->get('name');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}