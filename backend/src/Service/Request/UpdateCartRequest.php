<?php

namespace App\Service\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCartRequest implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="array", message="Тип значения {{ value }} должен быть {{ type }}.")
     */
    private $products;

    /**
     * UpdateCartRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->products = $request->get('products');
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }
}