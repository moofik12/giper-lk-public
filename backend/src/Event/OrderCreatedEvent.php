<?php

namespace App\Event;

use App\Entity\Cart;
use Symfony\Component\EventDispatcher\Event;

class OrderCreatedEvent extends Event
{
    public const NAME = 'order.created';

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * OrderCreatedEvent constructor.
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }
}