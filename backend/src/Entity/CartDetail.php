<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CartDetailRepository")
 */
class CartDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups("cart")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("cart")
     */
    private $article;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("cart")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups("cart")
     */
    private $retailPrice;

    /**
     * @ORM\Column(type="integer")
     * @Groups("cart")
     */
    private $priceTwo;

    /**
     * @ORM\Column(type="integer")
     * @Groups("cart")
     */
    private $priceThree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("cart")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups("cart")
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("cart")
     */
    private $isExistent;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $productOuterId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="cartDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cart;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticle(): string
    {
        return $this->article;
    }

    /**
     * @param string $article
     *
     * @return self
     */
    public function setArticle(string $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     */
    public function setCart(Cart $cart): void
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * @param mixed $retailPrice
     */
    public function setRetailPrice($retailPrice): void
    {
        $this->retailPrice = $retailPrice;
    }

    /**
     * @return int
     */
    public function getPriceTwo()
    {
        return $this->priceTwo;
    }

    /**
     * @param int $priceTwo
     */
    public function setPriceTwo($priceTwo): void
    {
        $this->priceTwo = $priceTwo;
    }

    /**
     * @return int
     */
    public function getPriceThree()
    {
        return $this->priceThree;
    }

    /**
     * @param int $priceThree
     */
    public function setPriceThree($priceThree): void
    {
        $this->priceThree = $priceThree;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getIsExistent()
    {
        return $this->isExistent;
    }

    /**
     * @param mixed $isExistent
     */
    public function setIsExistent($isExistent): void
    {
        $this->isExistent = $isExistent;
    }

    /**
     * @return string
     */
    public function getProductOuterId(): string
    {
        return $this->productOuterId;
    }

    /**
     * @param string $productOuterId
     */
    public function setProductOuterId(string $productOuterId): void
    {
        $this->productOuterId = $productOuterId;
    }
}
