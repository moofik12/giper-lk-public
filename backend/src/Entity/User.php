<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Security\User\PasswordAwareInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Serializable;
use App\Controller\Resource\UserSettings;

/**
 * @ApiResource(
 *     itemOperations={},
 *     collectionOperations={
 *     "special"={
 *         "method"="GET",
 *         "path"="/user/settings",
 *         "controller"=UserSettings::class,
 *         "swagger_context"={
 *              "summary"="Retrieves user settings and permissions"
 *          }
 *     }},
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"formats"={"jsonld"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface, PasswordAwareInterface, Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userErpId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pricelist", inversedBy="user")
     */
    private $pricelist;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="user")
     */
    private $carts;

    public function __construct()
    {
        $this->carts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param $username
     *
     * @return $this
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     *
     * @return $this
     */
    public function setPosition(string $position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return Pricelist|null
     */
    public function getPricelist(): ?Pricelist
    {
        return $this->pricelist;
    }

    /**
     * @param Pricelist $pricelist
     */
    public function setPricelist(Pricelist $pricelist): void
    {
        $this->pricelist = $pricelist;
    }

    /**
     * @return string
     */
    public function getUserErpId()
    {
        return $this->userErpId;
    }

    /**
     * @param string $userErpId
     */
    public function setUserErpId(string $userErpId): void
    {
        $this->userErpId = $userErpId;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCarts()
    {
        return $this->carts;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getSalt()
    {
        /* Соль не используется */
        return '';
    }

    public function eraseCredentials()
    {
        /* Нужно имплементить только в случае когда храним в токене незащищенные данные (plain password и т.п.) */
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->id,
            'username' => $this->username,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $unserializedTokenData = unserialize($serialized);

        $this->id = $unserializedTokenData['id'];
        $this->username = $unserializedTokenData['username'];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->username;
    }
}
