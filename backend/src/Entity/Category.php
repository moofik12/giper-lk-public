<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Resource\FilterSettings;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={
 *     "get",
 *     "special"={
 *         "method"="GET",
 *         "path"="/categories/{id}/filter/settings",
 *         "controller"=FilterSettings::class,
 *         "swagger_context"={
 *              "summary"="Retrieves filter settings for a category resource"
 *          }
 *     }},
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"formats"={"jsonld"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 * @ORM\HasLifecycleCallbacks()
 */
class Category implements \Serializable
{
    /**
     * @var int $id
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var int $parentId
     * @Groups({"read"})
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var string $name
     * @Groups({"read"})
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $categoryOuterId
     * @ORM\Column(name="category_outer_id", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $categoryOuterId;

    /**
     * @var string $parentOuterId
     * @ORM\Column(name="parent_outer_id", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $parentOuterId;

    /**
     * @var int $createdAt
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var int $updatedAt
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var Collection $products
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="category")
     * @ApiSubresource
     */
    private $products;

    /**
     * @var Collection $nestedProducts
     * @ORM\ManyToMany(targetEntity="App\Entity\Product")
     * @ApiSubresource
     */
    private $nestedProducts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->nestedProducts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     *
     * @return Category
     */
    public function setParentId(?int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Category
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreated(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     *
     * @return Category
     */
    public function setCreated(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface $updatedAt
     *
     * @return Category
     */
    public function setUpdated(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return Collection
     */
    public function getNestedProducts(): Collection
    {
        return $this->nestedProducts;
    }

    /**
     * @return string
     */
    public function getCategoryOuterId()
    {
        return $this->categoryOuterId;
    }

    /**
     * @param string $categoryOuterId
     */
    public function setCategoryOuterId($categoryOuterId): void
    {
        $this->categoryOuterId = $categoryOuterId;
    }

    /**
     * @return string
     */
    public function getParentOuterId()
    {
        return $this->parentOuterId;
    }

    /**
     * @param string $parentOuterId
     */
    public function setParentOuterId($parentOuterId): void
    {
        $this->parentOuterId = $parentOuterId;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->id,
            'name' => $this->name,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $unserializedData = unserialize($serialized);

        $this->id = $unserializedData['id'];
        $this->name = $unserializedData['name'];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
