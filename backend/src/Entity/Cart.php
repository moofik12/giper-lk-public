<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Controller\Resource\BlankResource;
use App\Controller\Operation\CreateCart;
use App\Controller\Operation\UpdateCart;
use App\Controller\Operation\SaveCart;
use App\Controller\Operation\CreateOrder;
use App\Controller\Resource\SavedCarts;
use App\Controller\Resource\OrderedCarts;
use App\Controller\Resource\LostCart;
use App\Controller\Operation\DeleteCart;
use App\Controller\Resource\CartResource;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "post"={
 *         "method"="POST",
 *         "path"="/carts",
 *         "controller"=CreateCart::class
 *      },
 *     "put"={
 *         "method"="PUT",
 *         "path"="/carts",
 *         "controller"=UpdateCart::class
 *      },
 *     "save"={
 *          "method"="POST",
 *          "path"="/carts/save",
 *          "controller"=SaveCart::class
 *     },
 *     "order"={
 *          "method"="POST",
 *          "path"="/orders",
 *          "controller"=CreateOrder::class
 *     },
 *     "lost"={
 *          "method"="GET",
 *          "path"="/carts/lost",
 *          "controller"=LostCart::class
 *     },
 *     "saved"={
 *          "method"="GET",
 *          "path"="/carts/saved",
 *          "controller"=SavedCarts::class
 *     },
 *     "ordered"={
 *          "method"="GET",
 *          "path"="/orders",
 *          "controller"=OrderedCarts::class
 *     }
 *     },
 *     itemOperations={
 *     "get"={
 *          "method"="GET",
 *          "path"="/carts/{id}",
 *          "controller"=CartResource::class
 *     },
 *     "delete"={
 *          "method"="DELETE",
 *          "path"="/carts/{id}",
 *          "controller"=DeleteCart::class
 *     }},
 *     normalizationContext={"groups"={"cart"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart implements \JsonSerializable
{
    /**
     * @Groups({"cart"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"cart"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Groups({"cart"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOrdered;

    /**
     * @Groups({"cart"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSaved;

    /**
     * @Groups({"cart"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Groups({"cart"})
     * @ORM\OneToMany(targetEntity="App\Entity\CartDetail", mappedBy="cart", fetch="EAGER")
     * @ApiSubresource()
     */
    private $cartDetails;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="carts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->cartDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsOrdered(): ?bool
    {
        return $this->isOrdered;
    }

    public function setIsOrdered(?bool $isOrdered): self
    {
        $this->isOrdered = $isOrdered;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|CartDetail[]
     */
    public function getCartDetails()
    {
        return $this->cartDetails;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIsSaved()
    {
        return $this->isSaved;
    }

    /**
     * @param bool $isSaved
     *
     * @return Cart
     */
    public function setIsSaved(bool $isSaved): self
    {
        $this->isSaved = $isSaved;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     */
    public function setIsDeleted($isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    public function jsonSerialize()
    {
        $serializedCartDetails = [];
        /** @var CartDetail $cartDetail */
        foreach ($this->cartDetails as $cartDetail) {
            $serializedCartDetails[] = [
                'id' => $cartDetail->getId(),
                'article' => $cartDetail->getArticle(),
                'quantity' => $cartDetail->getAmount(),
                'cart_id' => $cartDetail->getCartId()
            ];
        }

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'is_ordered' => $this->getIsOrdered(),
            'is_saved' => $this->getIsSaved(),
            'cart_details' => $serializedCartDetails
        ];
    }
}
