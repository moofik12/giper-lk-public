<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Filter\MultipleFulltextFilter;
use App\Filter\MultipleFulltextSearchAnnotation as FulltextSearchable;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"formats"={"jsonld"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"brand": "exact"})
 * @ApiFilter(RangeFilter::class, properties={"priceThree"})
 * @ApiFilter(BooleanFilter::class, properties={"actuality"})
 * @ApiFilter(MultipleFulltextFilter::class, properties={"search"})
 * @FulltextSearchable({"article", "barcode", "name"})
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="products")
 * @ORM\HasLifecycleCallbacks()
 */
class Product implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Groups({"read"})
     */
    private $name;

    /**
     * @ORM\Column(name="quantity", type="integer")
     * @Groups({"read"})
     */
    private $quantity;

    /**
     * @ORM\Column(name="package_quantity", type="integer")
     * @Groups({"read"})
     */
    private $packageQuantity;

    /**
     * @ORM\Column(name="retail_price", type="integer")
     * @Groups({"read"})
     */
    private $retailPrice;

    /**
     * @ORM\Column(name="price_two", type="integer")
     * @Groups({"read"})
     */
    private $priceTwo;

    /**
     * @ORM\Column(name="price_three", type="integer")
     * @Groups({"read"})
     */
    private $priceThree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $description = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $barcode;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $weight;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read"})
     */
    private $brand;

    /**
     * @ORM\Column(name="category_outer_id", type="string", length=255)
     * @Groups({"read"})
     */
    private $categoryOuterId;

    /**
     * @ORM\Column(name="restricted_access", type="boolean", length=255)
     * @Groups({"read"})
     */
    private $restrictedAccess;

    /**
     * @ORM\Column(name="actuality", type="boolean", length=255)
     * @Groups({"read"})
     */
    private $actuality;

    /**
     * @ORM\Column(name="article", type="string", length=255)
     * @Groups({"read"})
     */
    private $article;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $productOuterId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int|null
     */
    public function getPackageQuantity(): ?int
    {
        return $this->packageQuantity;
    }

    /**
     * @param int $packageQuantity
     *
     * @return Product
     */
    public function setPackageQuantity(int $packageQuantity): self
    {
        $this->packageQuantity = $packageQuantity;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRetailPrice(): ?int
    {
        return $this->retailPrice;
    }

    /**
     * @param int $retailPrice
     *
     * @return Product
     */
    public function setRetailPrice(int $retailPrice): self
    {
        $this->retailPrice = $retailPrice;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPriceThree(): ?int
    {
        return $this->priceThree;
    }

    /**
     * @param int $priceThree
     *
     * @return Product
     */
    public function setPriceThree(int $priceThree): self
    {
        $this->priceThree = $priceThree;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Product
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     *
     * @return Product
     */
    public function setBarcode(string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     *
     * @return Product
     */
    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int $value
     *
     * @return Product
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return Product
     */
    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriceTwo()
    {
        return $this->priceTwo;
    }

    /**
     * @param int $priceTwo
     *
     * @return $this
     */
    public function setPriceTwo(int $priceTwo): self
    {
        $this->priceTwo = $priceTwo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryOuterId()
    {
        return $this->categoryOuterId;
    }

    /**
     * @param mixed $categoryOuterId
     */
    public function setCategoryOuterId($categoryOuterId): void
    {
        $this->categoryOuterId = $categoryOuterId;
    }

    /**
     * @return mixed
     */
    public function getRestrictedAccess()
    {
        return $this->restrictedAccess;
    }

    /**
     * @param mixed $restrictedAccess
     */
    public function setRestrictedAccess($restrictedAccess): void
    {
        $this->restrictedAccess = $restrictedAccess;
    }

    /**
     * @return mixed
     */
    public function getActuality()
    {
        return $this->actuality;
    }

    /**
     * @param mixed $actuality
     */
    public function setActuality($actuality): void
    {
        $this->actuality = $actuality;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param $category
     *
     * @return $this
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    public function serialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'quantity' => $this->quantity,
            'package_quantity' => $this->packageQuantity,
            'retail_price' => $this->retailPrice,
            'price_two' => $this->priceTwo,
            'price_three' => $this->priceThree,
            'description' => $this->description,
            'barcode' => $this->barcode,
            'weight' => $this->weight,
            'value' => $this->value,
            'brand' => $this->brand,
            'category_outer_id' => $this->categoryOuterId,
            'restricted_access' => $this->restrictedAccess,
            'actuality' => $this->actuality,
            'article' => $this->article,
        ];
    }

    public function unserialize($serialized)
    {
        if (is_array($serialized)) {
            $this->id = $serialized['id'];
            $this->quantity = $serialized['quantity'];
            $this->packageQuantity = $serialized['package_quantity'];
            $this->retailPrice = $serialized['retail_price'];
            $this->priceTwo = $serialized['price_two'];
            $this->priceThree = $serialized['price_three'];
            $this->description = $serialized['description'];
            $this->barcode = $serialized[ 'barcode'];
            $this->weight = $serialized['weight'];
            $this->value = $serialized['value'];
            $this->brand = $serialized['brand'];
            $this->categoryOuterId = $serialized['category_outer_id'];
            $this->restrictedAccess = $serialized['restricted_access'];
            $this->actuality = $serialized['actuality'];
            $this->article = $serialized['article'];
            $this->name = $serialized['name'];
        }
    }

    /**
     * @return string
     */
    public function getProductOuterId(): string
    {
        return $this->productOuterId;
    }

    /**
     * @param string $productOuterId
     * @return Product
     */
    public function setProductOuterId(string $productOuterId): self
    {
        $this->productOuterId = $productOuterId;

        return $this;
    }
}
