<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180729082357 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        if ($this->connection->getDatabasePlatform()->getName() !== 'sqlite') {
            $this->write('SQLite migration skipped.');

            return;
        }

        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY NOT NULL, username VARCHAR(255) NOT NULL, gender VARCHAR(100) DEFAULT NULL, position VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE categories (id INTEGER PRIMARY KEY NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP DEFAULT NULL, updated_at TIMESTAMP DEFAULT NULL)');
        $this->addSql('CREATE TABLE products (id INTEGER PRIMARY KEY NOT NULL, article VARCHAR(255) NOT NULL, category_id INT DEFAULT 0, name VARCHAR(255) NOT NULL, quantity INT DEFAULT 0, package_quantity INT NOT NULL, retail_price INT NOT NULL, price_two INT NOT NULL, price_three INT NOT NULL, description TEXT DEFAULT NULL, barcode VARCHAR(255) DEFAULT NULL, weight INT DEFAULT NULL, value INT DEFAULT NULL, brand VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP DEFAULT NULL, updated_at TIMESTAMP DEFAULT NULL)');
    }

    public function down(Schema $schema) : void
    {
        if ($this->connection->getDatabasePlatform()->getName() !== 'sqlite') {
            $this->write('SQLite migration skipped.');

            return;
        }

        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE products');
    }
}
