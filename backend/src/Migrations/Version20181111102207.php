<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181111102207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cart ADD is_deleted BOOLEAN DEFAULT FALSE');
        $this->addSql('ALTER TABLE cart_detail ADD amount INTEGER NOT NULL');
        $this->addSql('ALTER TABLE cart_detail ADD article VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE cart_detail ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE cart_detail ADD retail_price INTEGER NOT NULL');
        $this->addSql('ALTER TABLE cart_detail ADD price_three INTEGER NOT NULL');
        $this->addSql('ALTER TABLE cart_detail ADD description VARCHAR(255)');
//        $this->addSql('ALTER TABLE cart_detail DROP COLUMN product_price');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN product_article');
        $this->addSql('ALTER TABLE cart_detail ADD COLUMN is_existent BOOLEAN DEFAULT TRUE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cart DROP COLUMN is_deleted');
        $this->addSql('ALTER TABLE cart_detail ADD quantity INTEGER NOT NULL');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN amount');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN article');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN name');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN retail_price');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN price_three');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN description');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN product_price');
        $this->addSql('ALTER TABLE cart_detail ADD product_article INTEGER NOT NULL');
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN is_existent');
    }
}
