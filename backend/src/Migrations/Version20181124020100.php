<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124020100 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE users ADD user_erp_id VARCHAR(255) DEFAULT ''");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE users DROP COLUMN user_erp_id');
    }
}
