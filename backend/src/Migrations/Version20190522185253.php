<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190522185253 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE products_tmp ADD COLUMN product_outer_id VARCHAR(255) DEFAULT '0'");
        $this->addSql("ALTER TABLE products ADD COLUMN product_outer_id VARCHAR(255) DEFAULT '0'");
        $this->addSql("ALTER TABLE cart_detail ADD COLUMN product_outer_id VARCHAR(255) DEFAULT '0'");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE cart_detail DROP COLUMN product_outer_id");
        $this->addSql("ALTER TABLE products DROP COLUMN product_outer_id");
        $this->addSql("ALTER TABLE products_tmp DROP COLUMN product_outer_id");
    }
}
