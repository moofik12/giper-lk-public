<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181125173424 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE cart_detail ADD COLUMN price_two INTEGER NOT NULL DEFAULT 0");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE cart_detail DROP COLUMN price_two");
    }
}
