<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110163603 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cart_detail DROP COLUMN product_price');
    }

    public function down(Schema $schema) : void
    {
       $this->addSql('ALTER TABLE cart_detail ADD product_price VARCHAR(255) NOT NULL');
    }
}
