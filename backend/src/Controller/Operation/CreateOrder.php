<?php

namespace App\Controller\Operation;

use App\Entity\Cart;
use App\Entity\User;
use App\Event\OrderCreatedEvent;
use App\Repository\CartRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CreateOrder
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage
    ) {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        /** @var CartRepository $cartRepository */
        $cartRepository = $this->entityManager->getRepository(Cart::class);
        $lastCart = $cartRepository->findLastCreatedCart($user);

        if (null === $lastCart || $lastCart->getIsOrdered() || $lastCart->getIsDeleted()) {
            throw new NotFoundHttpException('Нет корзины готовой для заказа');
        }

        $lastCart->setIsOrdered(true);
        $this->entityManager->flush();

        $orderCreated = new OrderCreatedEvent($lastCart);
        $eventDispatcher->dispatch(OrderCreatedEvent::NAME, $orderCreated);

        return [];
    }
}