<?php

namespace App\Controller\Operation;

use App\Entity\Cart;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DeleteCart
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Cart $data
     *
     * @return array
     */
    public function __invoke(Cart $data)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if ($data->getUser()->getId() !== $user->getId()) {
            throw new NotFoundHttpException('Корзины с таким ID не существует');
        }

        if (!$data->getIsDeleted()) {
            $data->setIsDeleted(true);
            $this->entityManager->flush();
        }

        return [];
    }
}