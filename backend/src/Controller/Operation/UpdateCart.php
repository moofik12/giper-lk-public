<?php

namespace App\Controller\Operation;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Service\Cart\CartException;
use App\Service\Cart\CartServiceInterface;
use App\Service\Request\UpdateCartRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UpdateCart
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var CartServiceInterface
     */
    private $cartService;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param CartServiceInterface $cartService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        CartServiceInterface $cartService
    ) {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->cartService = $cartService;
    }

    /**
     * @param UpdateCartRequest $request
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(UpdateCartRequest $request)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        /** @var CartRepository $cartRepository */
        $cartRepository = $this->entityManager->getRepository(Cart::class);
        $lastCart = $cartRepository->findLastCreatedCart($user);

        if (null === $lastCart || $lastCart->getIsOrdered() || $lastCart->getIsDeleted()) {
            throw new NotFoundHttpException('Не существует корзины которую можно было бы обновить');
        }

        try {
            $this->cartService->updateCart($lastCart, $request->getProducts());
        } catch (CartException $exception) {
            throw new UnprocessableEntityHttpException($exception->getMessage());
        }

        return [];
    }
}