<?php

namespace App\Controller\Operation;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Service\Request\SaveCartRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SaveCart
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param SaveCartRequest $request
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(SaveCartRequest $request)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        /** @var CartRepository $cartRepository */
        $cartRepository = $this->entityManager->getRepository(Cart::class);
        $lastCart = $cartRepository->findLastCreatedCart($user);

        if (null === $lastCart
            || $lastCart->getIsOrdered()
            || $lastCart->getIsSaved()
            || $lastCart->getIsDeleted()
        ) {
            throw new NotFoundHttpException('Не существует корзины которую можно было бы сохранить');
        }

        $lastCart->setIsSaved(true);
        $lastCart->setName($request->getName());
        $this->entityManager->flush();

        return [];
    }
}