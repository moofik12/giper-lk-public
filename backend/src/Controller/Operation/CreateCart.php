<?php

namespace App\Controller\Operation;

use App\Entity\User;
use App\Service\Cart\CartException;
use App\Service\Cart\CartService;
use App\Service\Request\UpdateCartRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CreateCart
{
    /**
     * @var
     */
    private $cart;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * CreateCart constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param CartService $cartService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        CartService $cartService
    ) {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->cartService = $cartService;
    }

    /**
     * @param UpdateCartRequest $request
     *
     * @return array
     */
    public function __invoke(UpdateCartRequest $request)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        try {
           $this->cart = $this->cartService->createCart($user, $request->getProducts());
        } catch (CartException $exception) {
            throw new UnprocessableEntityHttpException($exception->getMessage());
        }

        return [$this->cart];
    }
}