<?php

namespace App\Controller;

use App\Entity\Pricelist;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DownloadController extends Controller
{
    /**
     * @var TokenStorageInterface $tokenStorage
     *
     * @return JsonResponse|BinaryFileResponse
     *
     * @Route(
     *     name="api_pricelist_download",
     *     path="/api/pricelist/feed",
     *     methods={"GET"},
     * )
     */
    public function downloadFile(TokenStorageInterface $tokenStorage)
    {
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        /** @var Pricelist $pricelist */
        $pricelist = $user->getPricelist();

        try {
            if ($pricelist) {
                $filepath = $pricelist->getPath();
                $filepathExploded = explode('/', $filepath);
                $filename = end($filepathExploded);
                $response = new BinaryFileResponse($filepath);
                $response->headers->set('Content-Type', 'text/plain');
                $response->setContentDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                return $response;
            } else {
                $data = ['status' => 0, 'message' => 'File does not exist'];

                return new JsonResponse($data, 200);
            }
        } catch (\Exception $exception) {
            $data = ['status' => 0, 'message' => 'Download error'];

            return new JsonResponse($data, 400);
        }
    }
}
