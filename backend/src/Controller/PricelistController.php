<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\ERP\ERPApiInterface;
use App\Service\ERP\Pricelist\PricelistResolverInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PricelistController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ERPApiInterface
     */
    private $ERPApi;

    /**
     * @var PricelistResolverInterface
     */
    private $pricelistResolver;

    /**
     * PricelistController constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param ERPApiInterface $ERPApi
     * @param PricelistResolverInterface $pricelistResolver
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        ERPApiInterface $ERPApi,
        PricelistResolverInterface $pricelistResolver
    ) {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->ERPApi = $ERPApi;
        $this->pricelistResolver = $pricelistResolver;
    }

    /**
     * @Route("/api/pricelist", methods={"GET"})
     */
    public function getPricelist()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $username = $user->getUsername();
        $passwordHash = $user->getPassword();

        $availablePricelistsDto = $this->ERPApi->getAvailablePricelists($username, $passwordHash);
        $pricelistPath = $this->pricelistResolver->getPricelistFilePath($availablePricelistsDto);
        $response = new BinaryFileResponse($pricelistPath);;
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'price.xml'
        );

        return $response;
    }
}