<?php

namespace App\Controller\Resource;

use App\Entity\Pricelist;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserSettings
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return JsonResponse
     */
    public function __invoke()
    {
        $settings = [
            'permissions' => []
        ];

        $permissions = $this->getPricelistPermission();
        $settings['permissions'] = $permissions;

        return new JsonResponse($settings, 200);
    }

    /**
     * @return array
     */
    private function getPricelistPermission(): array
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        /** @var Pricelist $pricelist */
        $pricelist = $user->getPricelist();

        return ['pricelist_download' => (null === $pricelist)];
    }
}
