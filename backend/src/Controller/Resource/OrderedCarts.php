<?php

namespace App\Controller\Resource;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderedCarts
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param CartRepository $cartRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        CartRepository $cartRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->cartRepository = $cartRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return Cart[]|array
     */
    public function __invoke()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $orderedCarts = $this->cartRepository->findAllOrderedByUser($user);

        if (empty($orderedCarts)) {
            throw new NotFoundHttpException('У данного пользователя нет заказов');
        }

        return $orderedCarts;
    }
}