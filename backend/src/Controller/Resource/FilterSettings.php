<?php

namespace App\Controller\Resource;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class FilterSettings
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProductPriceRange constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Category $data
     *
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __invoke(Category $data)
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->entityManager->getRepository(Category::class);
        $categoryChildrenIds = $categoryRepository->getAllCategoryChildren($data->getId());
        $brands = $categoryRepository->getBrandsByIds($categoryChildrenIds);
        $priceRange = $categoryRepository->getPriceRangeById($categoryChildrenIds);

        $result = ['brands' => $brands, 'price_range' => $priceRange];

        return new JsonResponse($result, 200);
    }
}
