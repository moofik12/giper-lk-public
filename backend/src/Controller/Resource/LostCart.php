<?php

namespace App\Controller\Resource;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LostCart
{
    /**
     * @var cartRepository
     */
    private $cartRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSettings constructor.
     * @param cartRepository $cartRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        cartRepository $cartRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->cartRepository = $cartRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return Cart
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $lostCart = $this->cartRepository->findLastCreatedCart($user);

        if (null === $lostCart || $lostCart->getIsOrdered() || $lostCart->getIsSaved()) {
            throw new NotFoundHttpException('Нет брошенных корзин');
        }

        return $lostCart;
    }
}