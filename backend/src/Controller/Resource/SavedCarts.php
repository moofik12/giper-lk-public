<?php

namespace App\Controller\Resource;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Service\Cart\CartServiceInterface;
use App\Service\Cart\CartValidatorException;
use App\Service\Cart\CartValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SavedCarts
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var CartValidatorInterface
     */
    private $cartValidator;

    /**
     * UserSettings constructor.
     * @param EntityManagerInterface $entityManager
     * @param CartRepository $cartRepository
     * @param TokenStorageInterface $tokenStorage
     * @param CartValidatorInterface $cartValidator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CartRepository $cartRepository,
        TokenStorageInterface $tokenStorage,
        CartValidatorInterface $cartValidator
    ) {
        $this->entityManager = $entityManager;
        $this->cartRepository = $cartRepository;
        $this->tokenStorage = $tokenStorage;
        $this->cartValidator = $cartValidator;
    }

    /**
     * @return array
     */
    public function __invoke()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $savedCarts = $this->cartRepository->findAllSavedByUser($user);

        /* Уничтожаем все корзины у которых есть ссылки на несуществующий товар */
        foreach ($savedCarts as $savedCart) {
            try {
                $this->cartValidator->validateWithRelations($savedCart);
            } catch (CartValidatorException $exception) {
                $cartDetails = $exception->getNonexistentDetails();

                foreach ($cartDetails as $detail) {
                    $detail->setIsExistent(false);
                }
            }
        }

        $this->entityManager->flush();

        return $savedCarts;
    }
}