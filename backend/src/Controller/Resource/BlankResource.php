<?php

namespace App\Controller\Resource;


class BlankResource
{
    public function __invoke()
    {
        return [];
    }
}