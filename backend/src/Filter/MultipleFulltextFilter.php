<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Annotations\AnnotationException;
use HttpInvalidParamException;
use ReflectionException;
use ReflectionClass;

class MultipleFulltextFilter extends AbstractContextAwareFilter
{
    /**
     * @param string $property
     * @param $value
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array $context
     *
     * @throws HttpInvalidParamException
     * @throws AnnotationException
     * @throws ReflectionException
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    )
    {
        if ($property === 'search') {
            $this->logger->info('Search for: ' . $value);
        } else {
            return;
        }

        $reader = new AnnotationReader();
        $resourceReflection = new ReflectionClass(new $resourceClass);
        $annotation = $reader->getClassAnnotation(
            $resourceReflection,
            MultipleFulltextSearchAnnotation::class
        );

        if (!$annotation) {
            throw new HttpInvalidParamException('No Search implemented.');
        }

        $parameterName = $queryNameGenerator->generateParameterName($property);
        $search = [];

        foreach ($annotation->fields as $field) {
            $search[] = "LOWER(o.{$field}) LIKE LOWER(:{$parameterName})";
            $whereString = implode(' OR ', $search);
        }

        if ($whereString) {
            $queryBuilder->andWhere($whereString);
            $queryBuilder->setParameter($parameterName, '%' . $value . '%');
        }
    }

    /**
     * @param string $resourceClass
     *
     * @return array
     */
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["search_$property"] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Fulltext filter products using one or more fields',
                    'name' => 'search',
                    'type' => 'string',
                ],
            ];
        }

        return $description;
    }
}